import { DataConfigurationStrategy, SchemaLoaderStrategy, DefaultSchemaLoaderStrategy } from '@themost/data';
import { ExpressDataApplication, ExpressDataContext } from '@themost/express';
import { stringify as uuidStringify } from 'uuid';
import { parse as uuidParse } from 'uuid';
/**
 * Universis api
 * @type {Application}
 */
const app = require('../../../dist/server/app');
import { TestUtils } from '../../../dist/server/utils';
import path from 'path';
import { OneRosterService } from '../src/OneRosterService';
import { OneRosterOrgProvider, OneRosterOrg } from '../src/models/OneRosterOrg';
import { OneRosterAcademicSession, OneRosterAcademicSessionProvider } from '../src/models/OneRosterAcademicSession';
import { OneRosterClass, OneRosterClassProvider } from '../src/models/OneRosterClass';
import { OneRosterCourse, OneRosterCourseProvider } from '../src/models/OneRosterCourse';
import { OneRosterUser, OneRosterUserProvider } from '../src/models/OneRosterUser';
import { OneRosterEnrollment, OneRosterEnrollmentProvider } from '../src/models/OneRosterEnrollment';
const executeInTransaction = TestUtils.executeInTransaction;

const getBytes = num => {
    let b = new ArrayBuffer(4);
    new DataView(b).setUint32(0, num);
    return Array.from(new Uint8Array(b));
  }
  

describe('OneRosterSchema', ()=> {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    let currentDepartment;
    beforeAll(async () => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        app1.useService(OneRosterService);
        const loaders = app1.getConfiguration().getSourceAt('settings/schema/loaders');
        loaders.push({
            loaderType: path.resolve(__dirname, '../src/index') + '#OneRosterSchemaLoader'
        });
        app1.getConfiguration().setSourceAt('settings/schema/loaders', loaders);
        // reset data configuration
        app1.getConfiguration().useStrategy(SchemaLoaderStrategy, DefaultSchemaLoaderStrategy);
        context = app1.createContext();
        // get current department from env
        currentDepartment = process.env.UNIVERSIS_DEPARTMENT;
        
        await context.model('OneRosterOrgType').getItems();
        await context.model('OneRosterSessionType').getItems();
    });


    afterAll(async () => {
        await context.finalizeAsync();
    });

    it('should convert int to guid', async () => {
        const bytes = uuidParse('00000000-1001-1000-8000-000000000000');
        let intBytes = getBytes(2016);
        // replace bytes
        for (let index = 0; index < intBytes.length; index++) {
            bytes[index] = intBytes[index];
        }
        const str = uuidStringify(bytes, 0);
        expect(str).toBeTruthy();
        
    });

    it('should get org types', async () => {
        await executeInTransaction(context, async () => {
            const items = await context.model('OneRosterOrgType').silent().getItems();
            expect(items).toBeTruthy();
        });
    });

    it('should get orgs', async () => {
        await executeInTransaction(context, async () => {
            const filter = {
                academicYear: 2016,
                academicPeriod: 2
            };
            await context.model('OneRosterOrgType').silent().getItems();
            const orgs = await new OneRosterOrgProvider().preSync(context, filter);
            await context.model(OneRosterOrg).silent().upsert(orgs[0][1]);
            expect(orgs).toBeTruthy();
        });
    });

    it('should get academicSessions', async () => {
        await executeInTransaction(context, async () => {
            const filter = {
                academicYear: 2016,
                academicPeriod: 2
            };
            const academicSessions = await new OneRosterAcademicSessionProvider().preSync(context, filter);
            await context.model(OneRosterAcademicSession).silent().upsert(academicSessions[0][1]);
            expect(academicSessions).toBeTruthy();
        });
    });

    it('should get courses', async () => {

        await executeInTransaction(context, async () => {
            const filter = {
                academicYear: 2016,
                academicPeriod: 2,
                department: currentDepartment
            };
            const orgs = await new OneRosterOrgProvider().preSync(context, filter);
            await context.model(OneRosterOrg).silent().upsert(orgs[0][1]);
            const academicSessions = await new OneRosterAcademicSessionProvider().preSync(context, filter);
            await context.model(OneRosterAcademicSession).silent().upsert(academicSessions[0][1]);
            const courses = await new OneRosterCourseProvider().preSync(context, filter);
            expect(courses).toBeTruthy();
            await context.model(OneRosterCourse).silent().save(courses[0][1]);
        });
    });

    it('should get classes', async () => {
        await executeInTransaction(context, async () => {

            const filter = {
                academicYear: 2016,
                academicPeriod: 2,
                department: currentDepartment
            };

            const orgs = await new OneRosterOrgProvider().preSync(context, filter);
            await context.model(OneRosterOrg).silent().upsert(orgs[0][1]);

            const academicSessions = await new OneRosterAcademicSessionProvider().preSync(context, filter);
            await context.model(OneRosterAcademicSession).silent().upsert(academicSessions[0][1]);

            const courses = await new OneRosterCourseProvider().preSync(context, filter);
            await context.model(OneRosterCourse).silent().upsert(courses[0][1]);

            const classes = await new OneRosterClassProvider().preSync(context, filter);
            await context.model(OneRosterClass).silent().upsert(classes[0][1]);

            const users = await new OneRosterUserProvider().preSync(context, filter);
            await context.model(OneRosterUser).silent().upsert(users[0][1]);

            const enrollments = await new OneRosterEnrollmentProvider().preSync(context, filter);
            await context.model(OneRosterEnrollment).silent().upsert(enrollments[0][1]);
        });
    });

    fit('should sync data', async () => {
        await executeInTransaction(context, async () => {

            const filter = {
                academicYear: 2016,
                academicPeriod: 2,
                departments: [
                    170,
                    1010
                ]
            };

            await context.application.getService(OneRosterService).sync(context, {
                filter
            });
        });
    });
    

});