import {OneRosterFilterParser} from '../src/index';

describe('OneRosterFilterParser', ()=> {
    it('should parse equal operator', async () => {
        const parser = new OneRosterFilterParser();
        parser.source = 'title = \'Mathematics\'';
        parser.tokens = parser.toList();
        expect(parser.tokens).toBeTruthy();
        expect(parser.tokens[1].identifier).toEqual('eq');
        const str = parser.getString();
        expect(str).toEqual('title eq \'Mathematics\'');
    });

    it('should parse nested member', async () => {
        const parser = new OneRosterFilterParser();
        parser.source = 'course.title = \'Mathematics\'';
        parser.tokens = parser.toList();
        expect(parser.tokens).toBeTruthy();
        expect(parser.tokens[3].identifier).toEqual('eq');
        const str = parser.getString();
        expect(str).toEqual('course/title eq \'Mathematics\'');
    });

    it('should parse greater than operator', async () => {
        const parser = new OneRosterFilterParser();
        parser.source = 'id > 100';
        parser.tokens = parser.toList();
        expect(parser.tokens).toBeTruthy();
        expect(parser.tokens[1].identifier).toEqual('gt');
        const str = parser.getString();
        expect(str).toEqual('id gt 100');
    });

    it('should parse greater than or equal operator', async () => {
        const parser = new OneRosterFilterParser();
        parser.source = 'id >= 100';
        parser.tokens = parser.toList();
        expect(parser.tokens).toBeTruthy();
        expect(parser.tokens[1].identifier).toEqual('ge');
        expect(parser.getString()).toEqual('id ge 100');
    });

    it('should parse lower than operator', async () => {
        const parser = new OneRosterFilterParser();
        parser.source = 'id < 100';
        parser.tokens = parser.toList();
        expect(parser.tokens).toBeTruthy();
        expect(parser.tokens[1].identifier).toEqual('lt');
        expect(parser.getString()).toEqual('id lt 100');
    });

    it('should parse lower than or equal operator', async () => {
        const parser = new OneRosterFilterParser();
        parser.source = 'id <= 100';
        parser.tokens = parser.toList();
        expect(parser.tokens).toBeTruthy();
        expect(parser.tokens[1].identifier).toEqual('le');
        expect(parser.getString()).toEqual('id le 100');
    });

    it('should parse not equal operator', async () => {
        const parser = new OneRosterFilterParser();
        parser.source = 'status != \'closed\'';
        parser.tokens = parser.toList();
        expect(parser.tokens).toBeTruthy();
        expect(parser.tokens[1].identifier).toEqual('ne');
        expect(parser.getString()).toEqual('status ne \'closed\'');
    });

    it('should parse contains operator', async () => {
        const parser = new OneRosterFilterParser();
        parser.source = 'title ~ \'thematics\'';
        parser.tokens = parser.toList();
        expect(parser.tokens).toBeTruthy();
        expect(parser.tokens[0].identifier).toEqual('indexof');
        expect(parser.getString()).toEqual('indexof(title,\'thematics\') ge 0');
    });
});