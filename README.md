# @universis/one-roster

Universis OneRoster 1.2 middleware

### Installation

    npm i @universis/one-roster

## Configuration

Register `@universis/one-roster` under universis api server `services` section of application configuration:

```json    
    {
        "services": [
            {
                "serviceType": "@universis/one-roster#OneRosterService"
            }
        ]
    }
```

Add the `OneRosterSchemaLoader` under universis api server `schema/loaders` section of application configuration:

```json
{
  "schema": {
    "loaders": [
      { "loaderType": "@universis/one-roster#OneRosterSchemaLoader"}
    ]
  }
}
```

### Configure line items and results

Exporting line items and results of each course is optional and can be configured in application configuration:

```json
{
  "services": [
    {
      "serviceType": "@universis/one-roster#OneRosterLineItemProvider",
      "strategyType": "@universis/one-roster#OneRosterNoLineItemProvider",
    },
    {
      "serviceType": "@universis/one-roster#OneRosterResultProvider",
      "strategyType": "@universis/one-roster#OneRosterNoResultProvider",
    }
  ]
}
```

where `OneRosterNoLineItemProvider` and `OneRosterNoResultProvider` does not export line items and results respectively. This operation may be important for environments where one roster services are going be used for exporting data associated with course enrollments only e.g. Universis API server is used for exporting data to LMS.


### Configure class title format

`OneRosterClass` providers are using `classTitleFormat` to generate class titles. The default format is `"${title}"` -the title of the class-. The format can be changed in application configuration e.g.:

```json
{
    "settings": {
      "universis": {
        "one-roster": {
          "classTitleFormat": "${displayCode} ${title}"
        }
      }
    }
}
```

where the class provider will produce class titles in the format `"${displayCode} ${title}"` e.g. 101 Mathematics
The available variables are:
- `${id}` - the class identifier
- `${title}` - the title of the class
- `${course}` - the course identifier
- `${displayCode}` - the display code of the course
- `${academicYear}` - the academic year of the class
- `${academicPeriod}` - the academic period of the class
- `${department}` - the department identifier


### Configure class section title format

`OneRosterClass` providers are using `classSectionTitleFormat` to generate class titles if they are derived from course class sections. The default format is `"${title} - ${name}"`. The format can be changed in application configuration e.g.:

```json
{
    "settings": {
      "universis": {
        "one-roster": {
          "classSectionTitleFormat": "${displayCode} ${title} - ${name}"
        }
      }
    }
}
```

where the class provider will produce class titles in the format `"${displayCode} ${title} - ${name}"` e.g. 101 Mathematics - Section 1
The available variables are:
- `${id}` - the class identifier
- `${title}` - the title of the class
- `${name}` - the name of the class section
- `${section}` - the section identifier
- `${course}` - the course identifier
- `${displayCode}` - the display code of the course
- `${academicYear}` - the academic year of the class
- `${academicPeriod}` - the academic period of the class
- `${department}` - the department identifier

### Configure class part title format

`OneRosterClass` providers are using `classPartTitleFormat` to generate class titles if they are derived from course class parts. The default format is `"${title} - ${name}"`. The format can be changed in application configuration e.g.:

```json
{
    "settings": {
      "universis": {
        "one-roster": {
          "classPartTitleFormat": "${displayCode} ${title} - ${name}"
        }
      }
    }
}
```

where the class provider will produce class titles in the format `"${displayCode} ${title} - ${name}"` e.g. 101 Mathematics - Section 1
The available variables are:
- `${id}` - the class identifier
- `${title}` - the title of the class
- `${name}` - the name of the class part
- `${classIndex}` - the part identifier
- `${course}` - the course identifier
- `${displayCode}` - the display code of the course
- `${academicYear}` - the academic year of the class
- `${academicPeriod}` - the academic period of the class
- `${department}` - the department identifier

## Using @universis/robot

The `@universis/one-roster` package can be used with `@universis/robot` package to prepare and export OneRoster data. 

The following example demonstrates how to use `@universis/robot` package to export OneRoster data:

At the root director of `@universis/api` start the robot with the following command:

```bash
npx @universis/robot --config ./node_modules/@universis/one-roster/jobs/config.json
```

The robot will start and export OneRoster data by registering a new job for `@universis/robot` based on the configuration provided in `./node_modules/@universis/one-roster/config.json` file.

```json
[
  {
    "name": "one-roster-syncer",
    "interval": "20 minutes",
    "path": "./syncer.js"
  }
]
```

`syncer.js` job is trying to get classes that modified since the last time the job was run and exports them to the OneRoster API. The job is running by default every 20 minutes.

`@universis/robot` may be configured to run under `pm2` or any other process manager to ensure that the robot is running continuously.

```bash
pm2 start npx --name one-roster-syncer -- @universis/robot --config ./node_modules/@universis/one-roster/jobs/config.json 
```


