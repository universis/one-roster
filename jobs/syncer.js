const { getApplication } = require(process.cwd());
const { TraceUtils } = require('@themost/common');
const { OneRosterService, OneRosterFileLogger, AfterOneRosterClassListener } = require('@universis/one-roster');
const path = require('path');
const rfs = require('rotating-file-stream');
const moment = require('moment');
const { DataObjectState } = require('@themost/data');
const { QueryEntity, QueryExpression, QueryField } = require('@themost/query');
require('@themost/promise-sequence');

async function main() {

    let logDir = path.join(process.cwd(), 'log');
    // default log filename
    let accessLogFile;
    accessLogFile = 'class-syncer.log';
    TraceUtils.log(`Initializing master access log stream ${accessLogFile} for process [${process.pid}]`);
    // create a rotating write stream
    const logStream = rfs(accessLogFile, {
        interval: '1d', // rotate daily
        path: logDir
    });

    /**
     * @type {import('express').Application}
     */
    const container = getApplication();
    /**
     * @type {import('@themost/express').ExpressDataApplication}
     */
    const app = container.get('ExpressDataApplication');

    const context = app.createContext();

    const CourseClasses = context.model('CourseClass').silent();
    const ClassSyncerActions = context.model('ClassSyncerAction').silent();
    const OneRosterClasses = context.model('OneRosterClass').silent();
    // find last action
    const lastAction = await ClassSyncerActions.where('actionStatus/alternateName').equal('CompletedActionStatus')
        .orderByDescending('startTime').getItem();
    let lastModified = moment().subtract(1, 'days').toDate();
    if (lastAction && lastAction.lastModified) {
        lastModified = lastAction.lastModified;
    }
    // important note: use only academic year and omit academic period for filtering classes
    const $filter = '(year eq $it/department/currentYear)';
    const query = await CourseClasses.filterAsync({
        $filter: $filter,
        $orderby: 'dateModified desc',
        $top: 1
    });
    // important note: convert CourseClass.dateModified to datetimeoffset because there are some
    // implementations where it's a datetime field. 
    // $date() is an SQL dialect function implemented by data adapter for converting a datetime field to datetimeoffset
    // This is a workaround for this issue and it should be fixed in the future.
    const changes = await query.prepare().where(
        new QueryField({
            $value: {
                $toDate: [
                    new QueryField('dateModified').from(CourseClasses.viewAdapter),
                    'timestamp'
                ]
            }
        })
    ).greaterThan(lastModified).flatten().getAllItems();
    // get query e.g. SELECT * FROM CourseClass WHERE year = 2024 ORDER BY dateModified ASC
    const { query: queryClasses } = query;
    // try to get class replacements which are going to be processed along with the current classes
    const {query: queryReplacedBy} = context.model('CourseClasses').select();
    const { viewAdapter: OneRosterReplaceClasses } = context.model('OneRosterReplaceClass');
    // extend query and join with OneRosterReplaceClass in order to get parent classes
    // pseudo SQL: INNER JOIN OneRosterReplaceClassBase AS replaceClass ON CourseClassBase.id = replaceClass.courseClass
    queryClasses.join(
        new QueryEntity(OneRosterReplaceClasses).as('replaceClass')
    ).with(
        new QueryExpression().where(
            new QueryField('id').from(CourseClasses.viewAdapter)
        ).equal(
            new QueryField('courseClass').from('replaceClass')
        )
    );
    // delete order expression because is invalid in subqueries
    delete queryClasses.$order;

    const selectClasses = queryClasses.$select[CourseClasses.viewAdapter];
    // add parent class to select
    selectClasses.push(new QueryField('courseClass').from('replaceClass').as('replaceCourseClass'));
    selectClasses.push(new QueryField('replacedBy').from('replaceClass'));
    // pseudo SQL: INNER JOIN (SELECT ...) AS courseClass1 ON CourseClassBase.id = courseClass1.replacedBy
    queryReplacedBy.join(queryClasses.as('courseClass1')).with(
        new QueryExpression().where(
            new QueryField('id').from(CourseClasses.viewAdapter)
        ).equal(
            new QueryField('replacedBy').from('courseClass1')
        )
    )
    const selectReplacedBy = queryReplacedBy.$select[CourseClasses.viewAdapter];
    selectReplacedBy.push(new QueryField('replaceCourseClass').from('courseClass1'));
    // get parent classes (classes which are replacing other classes)
    const replacedByItems = await context.db.executeAsync(queryReplacedBy);
    // important note: exclude classes which are replaced by other classes
    // because they will be processed by the OneRosterService.sync() method
    const items = changes.filter((x) => {
        // get only classes which are not replaced by other classes
        return replacedByItems.find((y) => {
            return y.replaceCourseClass === x.id;
        }) == null;
    });
    // add classes which are replacing other classes (the sync operation will be executed for both)
    // important note: add classes which replace others to the beginning of the array in order to be processed first
    // iterate through replacedByItems and add to items array if not already exists
    for (const replacedByItem of replacedByItems) {
        const replacedByClass = items.find((x) => x.id === replacedByItem.id);
        if (replacedByClass == null) {
            items.unshift(replacedByItem);
        }
    }
    /**
     * @type {OneRosterService}
     */
    const service = app.getService(OneRosterService);
    const logger = new OneRosterFileLogger(logStream);
    let startTime = new Date();
    const alreadyProcessed = [];
    const actionStatus = { alternateName: 'ActiveActionStatus' };
    try {
        if (items.length > 0) {
            for (let index = 0; index < items.length; index++) {
                const item = items[index];
                // check if item has already been processed by the syncer
                if (alreadyProcessed.find(x => x.id === item.classCode)) {
                    // skip
                    // continue;
                }
                logger.log(context, 'ClassSyncerAction', `Synchronizing class ${item.id} ${item.title}`);
                await new Promise((resolve, reject) => {
                    // execute in unattended mode
                    context.unattended((cb) => {
                        // get filter params
                        const { department, course: courseCode, id: classCode, year: academicYear, period: academicPeriod } = item;
                        void service.sync(context, {
                            emitter: {
                                additionalType: 'ClassSyncerAction',
                                logger,
                            },
                            filter: {
                                department,
                                academicYear,
                                academicPeriod,
                                courseCode,
                                classCode
                            }
                        }, {
                            mode: 'partial'
                        }).then((results) => {
                            // find classes
                            const outputResult = results. find((x) => x[0] === 'OneRosterClass');
                             if (outputResult) {
                                const [, oneRosterClasses] = outputResult;
                                // add to already processed classes in order to by excluded from next synchronization
                                alreadyProcessed.push(...oneRosterClasses);
                            }
                            return Promise.resolve();
                        }).then(() => {
                            // continue with next
                            return cb();
                        }).catch((err) => {
                            // continue with error
                            return cb(err);
                        });
                    }, (err) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve();
                        }
                    });
                });
            }
            // send hooks
            await Promise.sequence(alreadyProcessed.map((oneRosterClass) => {
                return () => {
                    // send hook
                    const state = DataObjectState.Update;
                    const model = OneRosterClasses
                    const target = {
                        sourcedId: oneRosterClass.sourcedId,
                        school: oneRosterClass.school.sourcedId
                    }
                    logger.log(context, 'AfterOneRosterClassListener','Info', `Finalizing synchronization of ${oneRosterClass.classCode} ${oneRosterClass.title}`);
                    return new Promise((resolve) => {
                        new AfterOneRosterClassListener().afterSave({
                            model,
                            target,
                            state,
                            emitter: {
                                logger
                            }
                        }, (err) => {
                            if (err) {
                                logger.error(context, 'AfterOneRosterClassListener','Error', `An error occurred while finalizing synchronization of ${oneRosterClass.classCode} ${oneRosterClass.title}`);
                                logger.error(context, 'AfterOneRosterClassListener','Error', err.message, err.stack);
                            }
                            return resolve();
                        });
                    });
                }
            }));

        }
        if (items.length === 0) {
            logger.log(context, 'ClassSyncerAction', 'The class syncer job has been executed but there are no changes since last execution.');
        }
        actionStatus.alternateName = 'CompletedActionStatus';
    } catch (err) {
        actionStatus.alternateName = 'FailedActionStatus';
        // JSON.stringify(err, Object.getOwnPropertyNames(err))
        logger.error(context, 'ClassSyncerAction','Error', err.message, err.stack);
    }
    try {
        const endTime = new Date();
        // use last modified date of the first item because the changeset has been sorted by dateModified desc
        const lastModified = changes.length > 0 ? changes[0].dateModified : lastModified;
        await context.model('ClassSyncerAction').silent().save({
            lastModified,
            startTime,
            endTime,
            actionStatus
        });
    } catch (err) {
        // JSON.stringify(err, Object.getOwnPropertyNames(err))
        logger.error(context, 'ClassSyncerAction','Error', err.message, err.stack);
    }
    await context.finalizeAsync();
}

main().then(() => {
    process.exit();
}).catch((err) => {
    TraceUtils.error(err);
    process.exit(1);
});