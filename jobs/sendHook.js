const { getApplication } = require(process.cwd());
const { TraceUtils } = require('@themost/common');
const { OneRosterFileLogger, AfterOneRosterClassListener } = require('@universis/one-roster');
const path = require('path');
const rfs = require('rotating-file-stream');
const moment = require('moment');
const { DataObjectState } = require('@themost/data');
const minimist = require('minimist');
require('@themost/promise-sequence');

async function main() {

    const argv = minimist(process.argv.slice(2));

    let logDir = path.join(process.cwd(), 'log');
    // default log filename
    let accessLogFile;
    accessLogFile = 'class-syncer.log';
    TraceUtils.log(`Initializing master access log stream ${accessLogFile} for process [${process.pid}]`);
    // create a rotating write stream
    const logStream = rfs(accessLogFile, {
        interval: '1d', // rotate daily
        path: logDir
    });

    /**
     * @type {import('express').Application}
     */
    const container = getApplication();
    /**
     * @type {import('@themost/express').ExpressDataApplication}
     */
    const app = container.get('ExpressDataApplication');

    const context = app.createContext();

    const OneRosterClasses = context.model('OneRosterClass').silent();
    // find last n actions
    const a = argv.a || argv.action || 2;
    const ClassSyncerActions = context.model('ClassSyncerAction').silent();
    const lastActions = await ClassSyncerActions.where('actionStatus/alternateName').equal('CompletedActionStatus')
        .orderByDescending('startTime').take(a).select('id', 'lastModified').getItems();
    let lastModified = moment().subtract(1, 'days').toDate();
    if (lastActions.length) {
        const [firstAction] = lastActions.reverse();
        lastModified = firstAction.lastModified;
    }
    let items = [];
    const c = argv.c || argv.class;
    if (typeof c === 'string') {
        items = await OneRosterClasses.asQueryable().where((x, courseClass) => {
            return x.classCode === courseClass;
        }, c).getItems();
    } else {
        items = await OneRosterClasses.asQueryable().where((x, lastModified) => {
            return x.dateLastModified >= lastModified;
        }, lastModified).getItems();
    }
    const logger = new OneRosterFileLogger(logStream);
    logger.log(context, 'ClassSyncerAction','Info', `Found ${items.length} class(es) modified after ${lastModified.toISOString()}.`);
    try {
        if (items.length > 0) {
            logger.log(context, 'ClassSyncerAction','Info', `Sending hooks for ${items.length} class(es).`);
            // send hooks
            await Promise.sequence(items.map((oneRosterClass, index) => {
                return () => {
                    if ((index + 1) % 25 === 0) {
                        logger.log(context, 'ClassSyncerAction','Info', `Synchronizing ${index + 1} of ${items.length} classes.`);
                    }
                    // send hook
                    const state = DataObjectState.Update;
                    const model = OneRosterClasses
                    const target = {
                        sourcedId: oneRosterClass.sourcedId,
                        school: oneRosterClass.school.sourcedId
                    }
                    
                    logger.log(context, 'AfterOneRosterClassListener','Info', `Finalizing synchronization of ${oneRosterClass.classCode} ${oneRosterClass.title}`);
                    return new Promise((resolve) => {
                        new AfterOneRosterClassListener().afterSave({
                            model,
                            target,
                            state,
                            emitter: {
                                logger
                            }
                        }, (err) => {
                            if (err) {
                                logger.error(context, 'AfterOneRosterClassListener','Error', `An error occurred while finalizing synchronization of ${oneRosterClass.classCode} ${oneRosterClass.title}`);
                                logger.error(context, 'AfterOneRosterClassListener','Error', err.message, err.stack);
                            }
                            return resolve();
                        });
                    });
                }
            }));
        } else {
            logger.log(context, 'ClassSyncerAction','Info', `The operation has been completed successfully without getting any class modified after ${lastModified.toISOString()}.`);
        }
    } catch (err) {
        logger.error(context, 'ClassSyncerAction','Error', err.message, err.stack);
    }
    await context.finalizeAsync();
}

main().then(() => {
    process.exit();
}).catch((err) => {
    TraceUtils.error(err);
    process.exit(1);
});