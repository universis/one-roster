import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';

export declare class CourseReplacer extends ApplicationService {
    constructor(app: ExpressDataApplication);
}