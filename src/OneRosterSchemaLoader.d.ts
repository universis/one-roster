import {FileSchemaLoaderStrategy} from '@themost/data';
import { ConfigurationBase } from '@themost/common';
export declare class OneRosterSchemaLoader extends FileSchemaLoaderStrategy {
    constructor(config: ConfigurationBase);
}