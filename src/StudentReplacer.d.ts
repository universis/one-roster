import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';

export declare class StudentReplacer extends ApplicationService {
    constructor(app: ExpressDataApplication);
}