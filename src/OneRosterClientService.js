import superagent from 'superagent';
import { TraceUtils, HttpError, Args, HttpUnauthorizedError } from '@themost/common';

class OneRosterClientService {
    /**
     * @param {string} serviceRoot 
     */
    constructor(serviceRoot) {
        this.serviceRoot = serviceRoot;
        this.token = null;
    }

    async getOrg(sourcedId) {
        const {org} = await this.makeGetRequest(`/ims/oneroster/v1p1/orgs/${sourcedId}`, {
        });
        return org;
    }

    async getAllStudents() {
        const {users} = await this.makeGetRequest('/ims/oneroster/v1p1/students', {
        });
        return users;
    }

    async getStudent(sourcedId) {
        const {user} = await this.makeGetRequest(`/ims/oneroster/v1p1/students/${sourcedId}`, {
        });
        return user;
    }

    async getAllTeachers() {
        const {users} = await this.makeGetRequest('/ims/oneroster/v1p1/teachers', {
        });
        return users;
    }

    async getTeacher(sourcedId) {
        const {user} = await this.makeGetRequest(`/ims/oneroster/v1p1/teachers/${sourcedId}`, {
        });
        return user;
    }

    /**
     * @param {string} username 
     */
    async getStudentByUsername(username) {
        const {users} = await this.makeGetRequest('/ims/oneroster/v1p1/students', {
            filter: `username='${username}'`
        });
        return users && users[0];
    }

    /**
     * @param {string} sourcedId 
     */
    async getResultsForStudent(sourcedId) {
        const {results} = await this.makeGetRequest('/ims/oneroster/v1p1/results', {
            filter: `student='${sourcedId}'`
        });
        if (results && results.length) {
            for (const result of results) {
                if (result.lineItem) {
                    const lineItem = await this.getLineItem(result.lineItem.sourcedId);
                    if (lineItem) {
                        Object.assign(result.lineItem, lineItem);
                    }
                }
                
            }
        }
        return results;
    }

    async getClassesForStudent(sourcedId) {
        const {classes} = await this.makeGetRequest(`/ims/oneroster/v1p1/students/${sourcedId}/classes`, {
        });
        return classes;
    }

    async getGradingPeriod(sourcedId) {
        const {academicSession} = await this.makeGetRequest(`/ims/oneroster/v1p1/gradingPeriods/${sourcedId}`, {
        });
        return academicSession;
    }

    async getAllGradingPeriods() {
        const {academicSessions} = await this.makeGetRequest('/ims/oneroster/v1p1/gradingPeriods', {
        });
        return academicSessions;
    }

    /**
     * @param {string} sourcedId 
     */
    async getLineItem(sourcedId) {
        const value = await this.makeGetRequest(`/ims/oneroster/v1p1/lineItems/${sourcedId}`);
        if (value && value.lineItem && value.lineItem.class) {
            const courseClass = await this.getClass(value.lineItem.class.sourcedId);
            if (courseClass) {
                Object.assign(value.lineItem.class, courseClass);
            }
        }
        if (value && value.lineItem && value.lineItem.gradingPeriod) {
            const gradingPeriod = await this.getAcademicSession(value.lineItem.gradingPeriod.sourcedId);
            if (gradingPeriod) {
                Object.assign(value.lineItem.gradingPeriod, gradingPeriod);
            }
        }
        return value && value.lineItem;
    }

    async getClass(sourcedId) {
        const value = await this.makeGetRequest(`/ims/oneroster/v1p1/classes/${sourcedId}`);
        const courseClass = value.class;
        if (courseClass && courseClass.course) {
            const course = await this.getCourse(courseClass.course.sourcedId);
            if (course) {
                Object.assign(courseClass.course, course);
            }
        }
        if (courseClass && courseClass.school) {
            const school = await this.getSchool(courseClass.school.sourcedId);
            if (school) {
                Object.assign(courseClass.school, school);
            }
        }
        return courseClass;
    }

    async getSchool(sourcedId) {
        const {org} = await this.makeGetRequest(`/ims/oneroster/v1p1/schools/${sourcedId}`);
        return org;
    }

    async getCourse(sourcedId) {
        const { course } = await this.makeGetRequest(`/ims/oneroster/v1p1/courses/${sourcedId}`);
        if (course && course.org) {
            const org = await this.getSchool(course.org.sourcedId);
            if (org) {
                Object.assign(course.org, org);
            }
        }
        return course;
    }

    async getAcademicSession(sourcedId) {
        const { academicSession } = await this.makeGetRequest(`/ims/oneroster/v1p1/academicSessions/${sourcedId}`);
        if (academicSession && academicSession.parent) {
            const parent = await this.getAcademicSession(academicSession.parent.sourcedId);
            if (parent) {
                Object.assign(academicSession.parent, parent);
            }
        }
        return academicSession;
    }

    async makeGetRequest(url, queryParams) {
        try {
            Args.notNull(this.token, new HttpUnauthorizedError())
            const response = await superagent.get(new URL(url, this.serviceRoot).toString())
            .set('Authorization', `Bearer ${this.token.access_token}`)
            .set('accept', 'application/json')
            .query(queryParams);
            return response.body;
        } catch (error) {
            TraceUtils.error(error);
            if (error.response && error.response.body && error.response.body.message) {
                throw new HttpError(error.status, error.response.body.message);
            }
            throw new HttpError(error.status, error.message);
        } 
    }

    /**
     * @param {string} tokenURL
     * @param {{ grant_type: string, client_id: string, client_secret: string, scope: string }} params
     * @param {boolean=} force
     * @returns {Promise<this>}
     */
    async authorize(tokenURL, params, force) {
        if (force) {
            delete this.token;
        }
        if (this.token != null) {
            return this;
        }
        const response = await superagent.post(tokenURL).type('form').send(params);
        this.token = response.body;
        return this;
    }

}

export {
    OneRosterClientService
}