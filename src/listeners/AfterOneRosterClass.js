import { AfterOneRosterClassListener } from '../AfterOneRosterClassListener';

/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    const previous = event.previous;
    // handle status change to 'deleted'
    if (previous && previous.status !== event.target.status && event.target.status === 'tobedeleted') {
        // call afterRemove listener
        return new AfterOneRosterClassListener().afterRemove(event, callback);
    }
    return callback();
}