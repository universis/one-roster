import { AfterUserListener } from '../AfterUserListener';

/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return new AfterUserListener().afterSave(event, callback);
}

/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
export function afterRemove(event, callback) {
    return new AfterUserListener().afterRemove(event, callback);
}