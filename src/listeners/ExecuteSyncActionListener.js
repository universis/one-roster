import { TraceUtils } from "@themost/common";
import { DataObjectState, DataError } from "@themost/data";
import { OneRosterService } from "../OneRosterService";
import { OneRosterLogger } from '../models/OneRosterLogger';
/**
 * @param {DataEventArgs} event
 */
 async function afterSaveAsync(event) {
    let previousStatus = 'UnknownActionStatus';
    const context = event.model.context;
    const actionStatus = await context.model(event.model.name)
            .where('id').equal(event.target.id).select('actionStatus/alternateName').value();
    if (event.state === DataObjectState.Update) {
        if (event.previous && event.previous.actionStatus && event.previous.actionStatus.alternateName) {
            previousStatus = event.previous.actionStatus.alternateName;
        } else {
            throw new DataError('E_STATE', 'The previous state of an object cannot be determined', null, 'StudyProgramRegisterAction');
        }
    }
    if (actionStatus === 'ActiveActionStatus' && previousStatus !== 'ActiveActionStatus') {
        if (context.application.hasService(OneRosterService) === false) {
            throw new Error('OneRosterService is missing.');
        }
        // get initiator
        const initiator = await  context.model(event.model.name)
            .where('id').equal(event.target.id).silent().getTypedItem();
        // get departments
        let departments = [];
        if (Array.isArray(event.target.departments)) {
            departments.push(...event.target.departments);
        } else if (event.target.department) {
            departments.push(event.target.department);
        } else {
            departments = await initiator.property('departments').select('id').silent().take(-1).getItems();
        }
        // update start time by using an adhoc query
        const {id} = event.target;
        const startTime = new Date();
        await context.model('Action').silent().save({
            id,
            startTime
        });
        // assign start time for further processing
        Object.assign(event.target, {
            startTime
        });
        
        const newContext = context.application.createContext();
        Object.assign(newContext, {
            user: context.user
        });
        (async (context, initiator) => {
            const service = context.application.getService(OneRosterService);
            const logger = new OneRosterLogger();
            try {
                const filter = {
                    academicYear: initiator.academicYear,
                    academicPeriod: initiator.academicPeriod,
                    departments: departments.map((x) => x.id)
                };
                await service.sync(context, {
                    emitter: initiator,
                    filter
                });
                
                logger.info(context, 'OneRosterSyncAction', filter, `Completing synchronization of  ${filter.departments.length} department(s).`);
                await context.model(initiator.additionalType).silent().save({
                    id: initiator.id,
                    endTime: new Date(), // set last end time
                    actionStatus: {
                        alternateName: 'CompletedActionStatus'
                    }
                });
            } catch (err) {
                TraceUtils.error(err);
                logger.warn(context, 'OneRosterSyncAction', null, `The current synchronization failed with error: ${err.message}`);
                await context.model(initiator.additionalType).silent().save({
                    id: initiator.id,
                    endTime: new Date(), // set end time
                    actionStatus: {
                        alternateName: 'FailedActionStatus'
                    }
                });
            } finally {
                if (context && context.db) {
                    await context.finalizeAsync();
                }
            }
        })(
            newContext,
            initiator
        ).then(() => {
            // do nothing
        }).catch((err) => {
            TraceUtils.error(err);
        });
    }
 }
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}