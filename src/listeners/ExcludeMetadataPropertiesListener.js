/**
 * @param {{metadata?:*}} item
 * @param {string[]} properties
 */
function excludeMetadataProperties(item, properties) {
    if (item && item.metadata) {
        properties.forEach((key) => {
            delete item.metadata[key];
        });
    }
    if (item && item.metadata == null) {
        delete item.metadata;
    }
}

/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
 export function afterExecute(event, callback) {
     try {
        const context = event.model.context;
        if (event.query && event.query.$select) {
            // get properties
            const metadataAttribute = event.model.getAttribute('metadata');
            if (metadataAttribute == null) {
                return callback();
            }
            // get metadata model
            const metadataModel = context.model(metadataAttribute.type);
            // exclude id and additionalType
            const properties = [
                'id',
                'additionalType'
            ];
            if (metadataModel) {
                const property = metadataModel.attributes.find((attribute) => {
                    return attribute.type === event.model.name;
                });
                if (property) {
                    properties.push(property.name);
                }
            }
            if (Array.isArray(event.result)) {
                event.result.forEach((item) => excludeMetadataProperties(item, properties));
            } else if (typeof event.result === 'object') {
                excludeMetadataProperties(event.result, properties);
            }
        }
        return callback();
     } catch (err) {
         return callback(err);
     }
}