import { DataObjectState } from '@themost/data';
import { QueryExpression } from '@themost/query';

/**
 * @param {import('@themost/data').DataEventArgs} event
 */
async function afterSaveAsync(event) {
    try {
        if (event.state === DataObjectState.Update) {
            // get course classes for this instructor
            // important note: get course classes for the current academic year only 
            // excluding any filter for the current period
            const { context } = event.model;
            // for each courseClass set dateModified to now in order to include them in sync
            const q = await context.model('CourseClassInstructors').filterAsync('courseClass/year eq $it/courseClass/department/currentYear');
            const courseClassInstructors = await q.prepare().select('courseClass').where('instructor').equal(event.target.id).silent().flatten().getItems();
            const model = context.model('CourseClass');
            const { sourceAdapter: CourseClass } = model;
            for (const courseClassInstructor of courseClassInstructors) {
                // update dateModified for each course class
                const query = new QueryExpression().update(CourseClass).set({
                    dateModified: new Date()
                }).where('id').equal(courseClassInstructor.courseClass);
                await context.db.executeAsync(query, null);
            }
        }
    } finally {
        //
    }
}

export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}