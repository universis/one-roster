import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';

export declare class CourseClassReplacer extends ApplicationService {
    constructor(app: ExpressDataApplication);
}