import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';

export declare class InstructorReplacer extends ApplicationService {
    constructor(app: ExpressDataApplication);
}