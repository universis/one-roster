import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';

export declare class InstituteReplacer extends ApplicationService {
    constructor(app: ExpressDataApplication);
}