import {Router}  from 'express';
import { Guid } from '@themost/common';
import {promisify} from 'util';
import { NIL as NIL_UUID } from 'uuid';
import { entitySetRouter, TotalCount, OneRosterResponseFormatter } from './entitySetRouter';

function courseRouter(app) {
    const router = Router();

    router.use(entitySetRouter(app, 'OneRosterCourse'));

    router.get('/:id/classes', async function getClassesForCourse(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterClass');
            // apply filter
            const filterAsync = promisify(model.filter).bind(model);
            const query = await filterAsync(req.query);
            const results = await query.prepare().where('course').equal(id).silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    return router;
}

export {
    courseRouter
}