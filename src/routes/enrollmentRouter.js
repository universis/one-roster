import {Router}  from 'express';
import { entitySetRouter } from './entitySetRouter';

function enrollmentRouter(app) {
    const router = Router();

    router.use(entitySetRouter(app, 'OneRosterEnrollment'));
    
    return router;
}

export {
    enrollmentRouter
}