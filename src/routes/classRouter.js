import {Router}  from 'express';
import { Guid } from '@themost/common';
import { QueryEntity, QueryExpression, QueryField } from '@themost/query';
import {promisify} from 'util';
import { NIL as NIL_UUID } from 'uuid';
import { entitySetRouter, TotalCount, OneRosterResponseFormatter } from './entitySetRouter';

function classRouter(app) {
    const router = Router();

    router.use(entitySetRouter(app, 'OneRosterClass'));

    router.get('/:id/students', async function getStudentsForClass(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterUser');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const query = await filterAsync(req.query);
            const users = new QueryEntity(model.viewAdapter);
            const enrollments = new QueryEntity(req.context.model('OneRosterEnrollment').sourceAdapter).as('enrollments');
            query.select();
            query.query.join(enrollments).with(
                new QueryExpression().where(
                    new QueryField('user').from(enrollments)
                ).equal(
                    new QueryField('sourcedId').from(users)
                ).and(
                    new QueryField('class').from(enrollments)
                ).equal(
                    id
                ).and(
                    new QueryField('role').from(enrollments)
                ).equal(
                    'student'
                )
            )
            const results = await query.silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/teachers', async function getTeachersForClass(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterUser');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const query = await filterAsync(req.query);
            const users = new QueryEntity(model.viewAdapter);
            const enrollments = new QueryEntity(req.context.model('OneRosterEnrollment').sourceAdapter).as('enrollments');
            query.select();
            query.query.join(enrollments).with(
                new QueryExpression().where(
                    new QueryField('user').from(enrollments)
                ).equal(
                    new QueryField('sourcedId').from(users)
                ).and(
                    new QueryField('class').from(enrollments)
                ).equal(
                    id
                ).and(
                    new QueryField('role').from(enrollments)
                ).equal(
                    'teacher'
                )
            )
            const results = await query.silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/lineItems', async function getLineItemsForClass(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterLineItem');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const query = await filterAsync(req.query);
            /**
             * @type {{total:number,value:Array<*>}}
             */
            const result = await query.prepare().where('class').equal(id).silent().getList();
            res.set(TotalCount, result.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(result.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/lineItems/:lineItem/results', async function getResultsForLineItemForClass(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const lineItem = Guid.isGuid(req.params.lineItem) ? req.params.lineItem : NIL_UUID;
            const model = req.context.model('OneRosterResult');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const query = await filterAsync(req.query);
            /**
             * @type {{total:number,value:Array<*>}}
             */
            const result = await query.prepare().where('lineItem/class').equal(id).and('lineItem').equal(lineItem).silent().getList();
            res.set(TotalCount, result.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(result.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/students/:student/results', async function getResultsForStudentForClass(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const student = Guid.isGuid(req.params.student) ? req.params.student : NIL_UUID;
            const model = req.context.model('OneRosterResult');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const query = await filterAsync(req.query);
            /**
             * @type {{total:number,value:Array<*>}}
             */
            const result = await query.prepare().where('lineItem/class').equal(id).and('student').equal(student).silent().getList();
            res.set(TotalCount, result.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(result.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/results', async function getResultsForClass(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterResult');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const query = await filterAsync(req.query);
            /**
             * @type {{total:number,value:Array<*>}}
             */
            const result = await query.prepare().where('lineItem/class').equal(id).silent().getList();
            res.set(TotalCount, result.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(result.value));
        } catch (err) {
            return next(err);
        }
    });

    return router;
}

export {
    classRouter
}