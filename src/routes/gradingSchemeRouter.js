import {Router}  from 'express';
import { OneRosterUtils } from '../OnRosterUtils';

function generateId(id) {
    return `${OneRosterUtils.toHexString(id, '00000000')}-0000-1000-2001-000000000000`;
}

// eslint-disable-next-line no-unused-vars
function gradingSchemeRouter(app) {
    const router = Router();

    router.get('/', (req, res, next) => {
        req.context.model('GradeScale').asQueryable().silent().getAllItems().then((items) => {
            items.forEach((item) => {
                if (item.scaleType === 0) {
                    delete item.values;
                } else {
                    if (Array.isArray(item.values)) {
                        item.values.forEach((value) => {
                            delete value.gradeScale;
                            delete value.id;
                        })
                    }
                }                 
            });
            return res.json(items);
        }).catch((err) => {
            return next(err);
        });
    });
    
    router.post('/convert', (req, res, next) => {
        try {
            /**
             * @type {Function}
             */
            const GradeScale = req.context.model('GradeScale').getDataObjectType();
            /**
             * @type {{gradingScheme:*,score:*}}
             */
            const data = req.body;
            /**
             * @type {import('@themost/data').DataObject}
             */
            const gradeScale = Object.assign(new GradeScale(), data.gradingScheme);
            gradeScale.context = req.context;
            const value = gradeScale.convertTo(data.score);
            return res.json({
                value
            });
        } catch (error) {
            return next(error);
        }
    });
    
    return router;
}

export {
    generateId,
    gradingSchemeRouter
}