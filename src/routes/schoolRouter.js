import {Router}  from 'express';
import { Guid } from '@themost/common';
import { QueryEntity, QueryExpression, QueryField } from '@themost/query';
import {promisify} from 'util';
import { NIL as NIL_UUID } from 'uuid';
import { TotalCount, OneRosterResponseFormatter } from './entitySetRouter';
// eslint-disable-next-line no-unused-vars
/**
 * @param {import('@themost/data').DataApplication} app 
 * @returns {Express.Router}
 */
// eslint-disable-next-line no-unused-vars
function schoolRouter(app) {
    const router = Router();
    router.get('/', async function getAllSchools(req, res, next) {
        try {
            /**
             * @type {DataModel}
             */
            const model = req.context.model('OneRosterOrg');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {DataQueryable}
             */
            const query = await filterAsync(req.query);
            // get items
            const results = await query.prepare().and('type').equal('school').silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });
    router.get('/:id', async function getSchool(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterOrg');
            const item = await model
                .where('sourcedId').equal(id)
                .silent().getItem();
            return res.json(new OneRosterResponseFormatter().for(model).format(item));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/courses', async function getCoursesForSchool(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterCourse');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const query = await filterAsync(req.query);
            const results = await query.prepare()
                .and('org').equal(id).silent().getList();
                res.set(TotalCount, results.total);
                return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/classes', async function getClassesForSchool(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterClass');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const query = await filterAsync(req.query);
            const results = await query.prepare()
                .where('school').equal(id).silent().getList();
                res.set(TotalCount, results.total);
                return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/classes/:class_id/enrollments', async function getEnrollmentsForClassInSchool(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            // get class id
            const class_id = Guid.isGuid(req.params.class_id) ? req.params.class_id : NIL_UUID;
            // get enrollments
            const model = req.context.model('OneRosterEnrollment');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {DataQueryable}
             */
            const query = await filterAsync(req.query);
            const results = await query.prepare()
                .where('class').equal(class_id)
                .and('class/school').equal(id)
                .silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(req.context.model('OneRosterEnrollment')).format(results.value));
        } catch (err) {
            return next(err);
        }
    });


    router.get('/:id/classes/:class_id/students', async function getStudentsForClassInSchool(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            // get class id
            const class_id = Guid.isGuid(req.params.class_id) ? req.params.class_id : NIL_UUID;
            
            // get enrollments
            const model = req.context.model('OneRosterUser');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {DataQueryable}
             */
            const query = await filterAsync(req.query);

            const users = new QueryEntity(model.viewAdapter);
            const enrollments = new QueryEntity(req.context.model('OneRosterEnrollment').sourceAdapter).as('enrollments');
            query.select();
            query.query.join(enrollments).with(
                new QueryExpression().where(
                    new QueryField('user').from(enrollments)
                ).equal(
                    new QueryField('sourcedId').from(users)
                ).and(
                    new QueryField('class').from(enrollments)
                ).equal(
                    class_id
                ).and(
                    new QueryField('role').from(enrollments)
                ).equal(
                    'student'
                ).and(
                    new QueryField('school').from(enrollments)
                ).equal(
                    id
                )
            );
            const results = await query.silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(req.context.model('OneRosterUser')).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/classes/:class_id/teachers', async function getTeachersForClassInSchool(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            // get class id
            const class_id = Guid.isGuid(req.params.class_id) ? req.params.class_id : NIL_UUID;
            
            // get enrollments
            const model = req.context.model('OneRosterUser');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {DataQueryable}
             */
            const query = await filterAsync(req.query);

            const users = new QueryEntity(model.viewAdapter);
            const enrollments = new QueryEntity(req.context.model('OneRosterEnrollment').sourceAdapter).as('enrollments');
            query.select();
            query.query.join(enrollments).with(
                new QueryExpression().where(
                    new QueryField('user').from(enrollments)
                ).equal(
                    new QueryField('sourcedId').from(users)
                ).and(
                    new QueryField('class').from(enrollments)
                ).equal(
                    class_id
                ).and(
                    new QueryField('role').from(enrollments)
                ).equal(
                    'teacher'
                ).and(
                    new QueryField('school').from(enrollments)
                ).equal(
                    id
                )
            );
            const results = await query.silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(req.context.model('OneRosterUser')).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/students', async function getStudentsForSchool(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            // get enrollments
            const model = req.context.model('OneRosterUser');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {DataQueryable}
             */
            const query = await filterAsync(req.query);

            const users = new QueryEntity(model.viewAdapter);
            const enrollments = new QueryExpression().select(
                'user'
            ).from(req.context.model('OneRosterEnrollment').sourceAdapter)
                .where('role').equal('student').and('school').equal(id).distinct();
            if (query.query.$select == null) {
                query.select();
            }
            query.query.join(enrollments.as('enrollments')).with(
                new QueryExpression().where(
                    new QueryField('user').from('enrollments')
                ).equal(
                    new QueryField('sourcedId').from(users)
                )
            );
            const results = await query.silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(req.context.model('OneRosterUser')).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/teachers', async function getTeachersForSchool(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            // get enrollments
            const model = req.context.model('OneRosterUser');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {DataQueryable}
             */
            const query = await filterAsync(req.query);

            const users = new QueryEntity(model.viewAdapter);
            const enrollments = new QueryExpression().select(
                'user'
            ).from(req.context.model('OneRosterEnrollment').sourceAdapter)
                .where('role').equal('teacher').and('school').equal(id).distinct();
            if (query.query.$select == null) {
                query.select();
            }
            query.query.join(enrollments.as('enrollments')).with(
                new QueryExpression().where(
                    new QueryField('user').from('enrollments')
                ).equal(
                    new QueryField('sourcedId').from(users)
                )
            );
            const results = await query.silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(req.context.model('OneRosterUser')).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/enrollments', async function getEnrollmentsForSchool(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            // get enrollments
            const model = req.context.model('OneRosterEnrollment');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {DataQueryable}
             */
            const query = await filterAsync(req.query);
            const results = await query.prepare()
                .where('school').equal(id).silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/terms', async function getTermsForSchool(req, res, next) {
        try {
            // eslint-disable-next-line no-unused-vars
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterAcademicSession');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {DataQueryable}
             */
            const query = await filterAsync(req.query);
            const results = await query.prepare().where('type').equal('term').silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    return router;

}

export {
    schoolRouter
}