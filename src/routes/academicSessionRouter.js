import {Router}  from 'express';
import { entitySetRouter, TotalCount, OneRosterResponseFormatter } from './entitySetRouter';
import {promisify} from 'util';
import { Guid } from '@themost/common';
import { NIL as NIL_UUID } from 'uuid';

function academicSessionRouter(app) {
    const router = Router();

    router.use(entitySetRouter(app, 'OneRosterAcademicSession'));
    
    return router;
}

// eslint-disable-next-line no-unused-vars
function gradingPeriodRouter(app) {
    const router = Router();

    router.get('/', async function getAllGradingPeriods(req, res, next) {
        try {
            /**
             * @type {DataModel}
             */
            const model = req.context.model('OneRosterAcademicSession');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {DataQueryable}
             */
            const query = await filterAsync(req.query);
            /**
             * @type {{total:number,value:Array<*>}}
             */
            const results = await query.prepare().and('type').equal('gradingPeriod').silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id', async function getGradingPeriod(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterAcademicSession');
            const item = await model
                .where('sourcedId').equal(id)
                .and('type').equal('gradingPeriod')
                .silent().getItem();
            return res.json(new OneRosterResponseFormatter().for(model).format(item));
        } catch (err) {
            return next(err);
        }
    });
    return router;
}

// eslint-disable-next-line no-unused-vars
function termRouter(app) {
    const router = Router();

    router.get('/', async function getAllTerms(req, res, next) {
        try {
            /**
             * @type {DataModel}
             */
            const model = req.context.model('OneRosterAcademicSession');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {DataQueryable}
             */
            const query = await filterAsync(req.query);
            // get items
            const results = await query.prepare().and('type').equal('term').silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id', async function getTerm(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterAcademicSession');
            const item = await model
                .where('sourcedId').equal(id)
                .and('type').equal('term')
                .silent().getItem();
            return res.json(new OneRosterResponseFormatter().for(model).format(item));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/gradingPeriods', async function getGradingPeriodsForTerm(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterAcademicSession');
            const results = await model
                .where('parent').equal(id)
                .and('parent/type').equal('term')
                .and('type').equal('gradingPeriod')
                .silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/classes', async function getClassesForTerm(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model('OneRosterClass');
            const results = await model
                .where('terms/sourcedId').equal(id)
                .silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });
    return router;
}

export {
    academicSessionRouter,
    gradingPeriodRouter,
    termRouter
}