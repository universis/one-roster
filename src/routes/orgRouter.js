import {Router}  from 'express';
import { entitySetRouter } from './entitySetRouter';

function orgRouter(app) {
    const router = Router();

    router.use(entitySetRouter(app, 'OneRosterOrg'));
    
    return router;
}

export {
    orgRouter
}