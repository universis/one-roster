import {Router}  from 'express';
import { entitySetRouter, TotalCount, OneRosterResponseFormatter } from './entitySetRouter';
import { QueryEntity, QueryExpression, QueryField } from '@themost/query';
import { OneRosterGuidRef } from '../models/OneRosterGuidRef';
import {promisify} from 'util';
import { Guid } from '@themost/common';
import { NIL as NIL_UUID } from 'uuid';


function userRouter(app) {
    const router = Router();

    router.use((req, res, next) => {
        Object.assign(req.query, {
            $select: '*',
            $expand: [
                OneRosterGuidRef.selectFrom('orgs')
            ].join(',')
        });
        return next();
    }, entitySetRouter(app, 'OneRosterUser'));

    router.get('/:id/classes', async function getClassesForUser(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;

            const model = req.context.model('OneRosterClass');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const query = await filterAsync(req.query);

            const classes = new QueryEntity(model.viewAdapter);
            const enrollments = new QueryEntity(req.context.model('OneRosterEnrollment').sourceAdapter).as('enrollments');
            query.select();
            query.query.join(enrollments).with(
                new QueryExpression().where(
                    new QueryField('class').from(enrollments)
                ).equal(
                    new QueryField('sourcedId').from(classes)
                ).and(
                    new QueryField('user').from(enrollments)
                ).equal(
                    id
                )
            )
            const results = await query.silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });
    
    return router;
}

export {
    userRouter
}