import {Router}  from 'express';
import { Guid, Args } from '@themost/common';
import {promisify} from 'util';
import { NIL as NIL_UUID } from 'uuid';

const TotalCount = 'X-Total-Count';

function entitySetRouter(app, entityType) {
    const router = Router();

    router.use(function setDefaultQueryOptions(req, res, next) {
        Object.assign(req.query, {
            $select: null,
            $expand: null
        });
        return next();
    });

    router.get('/', async function getItems(req, res, next) {
        try {
            /**
             * @type {DataModel}
             */
            const model = req.context.model(entityType);
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {DataQueryable}
             */
            const query = await filterAsync(req.query);
            // get items
            const result = await query.silent().getList();
            res.set('X-Total-Count', result.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(result.value));
        } catch (err) {
            return next(err);
        }
    });
    router.get('/:id', async function getItem(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            const model = req.context.model(entityType);
            // apply expand and select (if any)
            const filterAsync = promisify(model.filter).bind(model);
            const query = await filterAsync({
                $select: req.query.$select,
                $expand: req.query.$expand
            });
            const item = await query.where('sourcedId').equal(id).silent().getItem();
            res.set('X-Total-Count', item != null ? 1 : 0);
            return res.json(new OneRosterResponseFormatter().for(model).format(item));
        } catch (err) {
            return next(err);
        }
    });
    return router;
}

class OneRosterResponseFormatter {
    constructor() {
        /**
         * @param {DataModel} thisModel 
         * @returns *
         */
        this.for = (thisModel) => {
            return {
                /**
                 * @param {*} data
                 */
                format(data){
                    const res = {};
                    if (Array.isArray(data)) {
                        Args.check(thisModel.collectionName != null, new Error('Invalid format parameter'));
                        Object.defineProperty(res, thisModel.collectionName, {
                            configurable: true,
                            enumerable: true,
                            writable: true,
                            value: data
                        });
                        return res
                    }
                    Args.check(thisModel.alternateName != null, new Error('Invalid format parameter'));
                    Object.defineProperty(res, thisModel.alternateName, {
                        configurable: true,
                        enumerable: true,
                        writable: true,
                        value: data
                    });
                    return res;
                }
            }
        }
    }
}

export {
    entitySetRouter,
    TotalCount,
    OneRosterResponseFormatter
}