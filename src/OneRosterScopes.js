const OneRosterScopes = [
    {
        scope: 'https://purl.imsglobal.org/spec/or/v1p1/scope/roster-core.readonly',
        description: 'enable access to the getAcademicSession, getClass, getCourse, getEnrollment, getGradingPeriod, getOrg, getSchool, getStudent, getTeacher, getUser, getAllAcademicSessions, getAllClasses, getAllCourses, getAllEnrollments, getAllGradingPeriods, getAllOrgs, getAllSchools, getAllStudents, getAllTeachers and getAllUsers rostering endpoints'    
    },
    {
        scope: 'https://purl.imsglobal.org/spec/or/v1p1/scope/roster.readonly',
        description: 'enable access to ALL of the rostering endpoints EXCEPT getDemographic and getDemographics;'
    },
    {
        scope: 'https://purl.imsglobal.org/spec/or/v1p1/scope/roster-demographics.readonly',
        description: 'enable access to the getDemographics and getAllDemographics rostering endpoints;'
    },
    {
        scope: 'https://purl.imsglobal.org/spec/or/v1p1/scope/resource.readonly',
        description: 'enable access to the getResource, getAllResources, ghetResourcesForClass and getResourcesForCourse resources endpoints;'
    },
    {
        scope: 'https://purl.imsglobal.org/spec/or/v1p1/scope/gradebook.readonly',
        description: 'enable access to the getCategory, getAllCategories, getLineItem, getAllLineItems, getResult, getAllResults, getLineItemsForClass, getResultsForClass, getResultsForLineItemForClass and getResultsForStudentForClass gradebook endpoints;'
    },
    {
        scope: 'https://purl.imsglobal.org/spec/or/v1p1/scope/gradebook.createput',
        description: 'enable access to the getCategory, getAllCategories, getLineItem, getAllLineItems, getResult, getAllResults, getLineItemsForClass, getResultsForClass, getResultsForLineItemForClass and getResultsForStudentForClass gradebook endpoints;enable access to the putCategory, putLineItem and putResult gradebook endpoints;'
    },
    {
        scope: 'https://purl.imsglobal.org/spec/or/v1p1/scope/gradebook.delete',
        description: 'enable access to the deleteCategory, deleteLineItem and deleteResult gradebook endpoints;'
    }
];

export {
    OneRosterScopes
};