
import { DataNotFoundError } from '@themost/common';
import { DataObjectState } from '@themost/data';
import { OneRosterLogger } from '@universis/one-roster';
import request from 'superagent';

async function afterSaveOrRemoveAsync(event) {
    const { target, model: { context } } = event;
    let state;
    let item;
    if (event.state === DataObjectState.Delete) {
        if (event.previous == null) {
            throw new DataNotFoundError('The previous state of the item cannot be determined.', null, event.model.name);
        }
        // get deleted item
        item = event.previous;
        state = 'delete';
    } else {
        // get original item
        item = await context.model('OneRosterClass').where('sourcedId').equal(target.sourcedId).silent().flatten().getItem();
        if (item == null) {
            throw new DataNotFoundError('The specified item cannot be found.', null, event.model.name);
        }
        state = event.state === DataObjectState.Insert ? 'create' : 'update';
    }
    const logger = (event.emitter && event.emitter.logger) || new OneRosterLogger();
    // get school and sourcedId
    const { school, sourcedId } = item;
    // get web hoooks from application configuration
    // todo:: add OneRoster hooks to database
    /**
     * @type {Array<{name: string, active: boolean, events: string[], config: { url:string,contentType: string,accept:string,insecure: boolean }}>}
     */
    const hooks = context.getConfiguration().getSourceAt('settings/universis/one-roster/hooks') || [];
    if (hooks.length === 0) {
        return;
    }
    // send webhooks (no wait)
    await Promise.all(hooks.map((hook) => {
        if (hook.active && hook.events.includes(state)) {
            // send webhook, get configuration
            let { accept, contentType, url, httpMethod, secret } = hook.config;
            // set default content type
            httpMethod = httpMethod || 'POST';
            // create request
            const req =  request(httpMethod, url).set('Content-Type', contentType)
            // set accept header
            if (accept) {
                req.set('Accept', accept);
            }
            // set supergent request type
            if (contentType === 'application/x-www-form-urlencoded') {
                req.type('form');
            }
            // set secret if any
            if (secret) {
                req.set('X-OneRoster-Secret', secret);
            }
            // send hook
            return req.send({
                    school,
                    sourcedId
                });
        }
        return Promise.resolve();
    })).then(
        /**
         * @param {import('superagent').Response} result 
         */
        ([result]) => {
        if (result.ok === false) {
            logger.error(context, 'Webhook','Error', `Failed to send webhook for ${item.classCode} ${item.title} to consumer. The response status is not OK and the response contains information about the error.`);
            logger.error(context, 'Webhook','Error', 'Response', result);
        }
        if (result.body && !result.body.result) {
            logger.error(context, 'Webhook','Error', `Failed to send webhook for ${item.classCode} ${item.title} to consumer. The response body contains information about the error.`);
            logger.error(context, 'Webhook','Error', result.body);
        } else {
            logger.log(context, 'Webhook','Info', `Webhook for ${item.classCode} ${item.title} sent successfully to consumer.`);
        }
    }).catch((err) => {
        logger.error(context, 'Webhook','Error', `Failed to send webhook for ${item.classCode} ${item.title} to consumer with an error.`);
        logger.error(context, 'Webhook','Error', err.message, err.stack);
    });

}

class AfterOneRosterClassListener {
    /**
     * @param {import('@themost/data').DataEventArgs} event
     * @param {Function} callback
     */
    afterSave(event, callback) {
        void afterSaveOrRemoveAsync(event).then(() => {
            return callback();
        }).catch((err) => {
            return callback(err);
        });
    }
    /**
     * @param {import('@themost/data').DataEventArgs} event
     * @returns {Promise<void>}
     */
    afterSaveAsync(event) {
        return afterSaveOrRemoveAsync(event);
    }
    /**
     * @param {import('@themost/data').DataEventArgs} event
     * @param {Function} callback
     */
    afterRemove(event, callback) {
        void afterSaveOrRemoveAsync(event).then(() => {
            return callback();
        }).catch((err) => {
            return callback(err);
        });
    }
}

export {
    AfterOneRosterClassListener
}