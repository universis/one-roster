import { DataNotFoundError } from '@themost/common';
import { DataObjectState } from '@themost/data';
import { QueryExpression } from '@themost/query';
import { promisify } from 'util';
import { v4 } from 'uuid';

class AfterUserListener {
    
    /**
     * @param {import('@themost/data').DataEventArgs} event 
     */
    async afterSaveAsync(event) {
        // exit on insert
        if (event.state === DataObjectState.Insert) {
            return;
        }
        // get context
        const { context } = event.model;
        // check if OneRosterUser  is available
        let model = context.model('OneRosterUser');
        if (model == null) {
            return;
        }
        // get previous state
        const { previous } = event;
        if (previous == null) {
            throw new DataNotFoundError('The previous state of an object cannot be determined', null, event.model.name);
        }
        // check if previous username is different than current
        if (event.target.name != null && previous.name != event.target.name) {
            /**
             * @type {function(query):Promise<*>}
             */
            const executeAsync = promisify(context.db.execute.bind(context.db));
            const { sourceAdapter: OneRosterUsers } = model;
            // update username
            // pseudoSQL: UPDATE OneRosterUsers SET username = @current WHERE username = @previous
            await executeAsync(
                new QueryExpression().update(OneRosterUsers).set({
                    username: event.target.name,
                    dateLastModified: new Date()
                }).where('username').equal(previous.name), null
            );

            // get current user's StudentCourseClasses
            const q = await context.model('StudentCourseClasses').filterAsync('courseClass/year eq $it/student/department/currentYear and courseClass/period eq $it/student/department/currentPeriod');
            const studentCourseClasses = await q.prepare().select('courseClass').where('student/user/name').equal(event.target.name).silent().flatten().getItems();

            model = context.model('CourseClass');
            const { sourceAdapter: CourseClass } = model;

            // if there user has StudentCourseClasses
            if (studentCourseClasses.length > 0) {
                // for each courseClass set dateModified to now in order for syncer to take them into account
                for (const studentCourseClass of studentCourseClasses) {
                    await executeAsync(
                        new QueryExpression().update(CourseClass).set({
                            dateModified: new Date()
                        }).where('id').equal(studentCourseClass.courseClass), null
                    );
                }
            } else {
                // check for CourseClassInstructors
                const q = await context.model('CourseClassInstructors').filterAsync('courseClass/year eq $it/courseClass/department/currentYear and courseClass/period eq $it/courseClass/department/currentPeriod');
                const courseClassInstructors = await q.prepare().select('courseClass').where('instructor/user/name').equal(event.target.name).silent().flatten().getItems();
                // for each courseClass set dateModified to now in order for syncer to take them into account
                for (const courseClassInstructor of courseClassInstructors) {
                    await executeAsync(
                        new QueryExpression().update(CourseClass).set({
                            dateModified: new Date()
                        }).where('id').equal(courseClassInstructor.courseClass), null
                    );
                }
            }
        }
    }

    async afterRemoveAsync(event) {
        // get context
        const { context } = event.model;
        // check if OneRosterUser  is available
        let model = context.model('OneRosterUser');
        if (model == null) {
            return;
        }
        // get previous state
        const { previous } = event;
        if (previous == null) {
            throw new DataNotFoundError('The previous state of an object cannot be determined', null, event.model.name);
        }
        const { sourceAdapter: OneRosterUsers } = model;
        /**
         * @type {function(query):Promise<*>}
         */
        const executeAsync = promisify(context.db.execute.bind(context.db));
        await executeAsync(
            new QueryExpression().update(OneRosterUsers).set({
                username: v4().toString(),
                status: 'tobedeleted',
                dateLastModified: new Date()
            }).where('username').equal(previous.name), null
        );

        // get current user's StudentCourseClasses
        const q = await context.model('StudentCourseClasses').filterAsync('courseClass/year eq $it/student/department/currentYear and courseClass/period eq $it/student/department/currentPeriod');
        const studentCourseClasses = await q.prepare().select('courseClass').where('student/user/name').equal(event.previous.name).silent().flatten().getItems();

        model = context.model('CourseClass');
        const { sourceAdapter: CourseClass } = model;

        // if there user has StudentCourseClasses
        if (studentCourseClasses.length > 0) {
            // for each courseClass set dateModified to now in order for syncer to take them into account
            for (const studentCourseClass of studentCourseClasses) {
                await executeAsync(
                    new QueryExpression().update(CourseClass).set({
                        dateModified: new Date()
                    }).where('id').equal(studentCourseClass.courseClass), null
                );
            }
        } else {
            // check for CourseClassInstructors
            const q = await context.model('CourseClassInstructors').filterAsync('courseClass/year eq $it/courseClass/department/currentYear and courseClass/period eq $it/courseClass/department/currentPeriod');
            const courseClassInstructors = await q.prepare().select('courseClass').where('instructor/user/name').equal(event.previous.name).silent().flatten().getItems();
            // for each courseClass set dateModified to now in order for syncer to take them into account
            for (const courseClassInstructor of courseClassInstructors) {
                await executeAsync(
                    new QueryExpression().update(CourseClass).set({
                        dateModified: new Date()
                    }).where('id').equal(courseClassInstructor.courseClass), null
                );
            }
        }
    }

    /**
     * 
     * @param {import('@themost/data').DataEventArgs} event 
     * @param {function(err?: Error)} callback 
     */
    afterSave(event, callback) {
        void AfterUserListener.prototype.afterSaveAsync(event).then(() => {
            callback();
        }).catch((err) => {
            callback(err);
        });
    }

    /**
     * 
     * @param {import('@themost/data').DataEventArgs} event 
     * @param {function(err?: Error)} callback 
     */
    afterRemove(event, callback) {
        void AfterUserListener.prototype.afterRemoveAsync(event).then(() => {
            callback();
        }).catch((err) => {
            callback(err);
        });
    }
}

export {
    AfterUserListener
}
