import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';
import { Request } from 'express';

export declare class OneRosterScopeAccess extends ApplicationService {
    
    elements: { scope: string[], resource: string, access: string[] }[];
    
    constructor(app: ExpressDataApplication);

    verify(req: Request): Promise<boolean>;

}