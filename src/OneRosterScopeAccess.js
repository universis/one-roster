import { ApplicationService, Args, TraceUtils } from '@themost/common';
import scopeAccessElements from './config/scope.access';
import url from 'url';
import { checkSync } from 'recheck';
import { ScopeString } from '@universis/janitor';

class OneRosterScopeAccess extends ApplicationService {
    constructor(app) {
        super(app);
        Object.defineProperty(this, 'elements', {
            configurable: true,
            enumerable: true,
            writable: false,
            value: scopeAccessElements.map((x) => {
                const resource = x.resource.startsWith('^') ? "^" + x.resource : x.resource;
                const regex = new RegExp(resource, 'i');
                Object.assign(x, {
                    resource,
                    regex
                });
                return x;
            }).filter((x) => {
                const result = checkSync(x.resource, 'i');
                if (result.status !== 'safe') {
                    TraceUtils.error(`Resource path "${x.resource}" regex is marked as ${result.status}.`, 'OneRosterScopeAccess');
                    return false;
                }
                return true;
            })
        });

    }
    verify(req) {
        return new Promise((resolve, reject) => {
            try {
                // validate request context
                Args.notNull(req.context,'Context');
                // validate request context user
                Args.notNull(req.context.user,'User');
                if (req.context.user.authenticationScope && req.context.user.authenticationScope.length>0) {
                    // get original url
                    let requestUrl = url.parse(req.originalUrl).pathname;
                    // get user context scopes as array e.g, ['students', 'students:read']
                    let requestScopes = new ScopeString(req.context.user.authenticationScope).split();
                    // get user access based on HTTP method e.g. GET -> read access
                    let requestAccess = /^\b(POST|PUT|PATCH|DELETE)\b$/i.test(req.method) ? "write" : "read";
                    const element = this.elements.filter((x) => {
                        return x.scope.find((y) => {
                            // search user scopes (validate wildcard scope)
                            return y === "*" || requestScopes.indexOf(y)>=0;
                        })
                    }).find((x) => {
                        return x.regex.test(requestUrl);
                    });
                    // the first element found defines explicitly access level to the given resource
                    if (element && element.access.indexOf(requestAccess) >= 0) {
                        return resolve(element);
                    }
                }
                return resolve();
            }
            catch(err) {
                return reject(err);
            }
        });
    }
}

export {
    OneRosterScopeAccess
}