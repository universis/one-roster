import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';

export declare class StudentCourseClassReplacer extends ApplicationService {
    constructor(app: ExpressDataApplication);
}
