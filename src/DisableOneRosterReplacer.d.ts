import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';

export declare class DisableOneRosterReplacer extends ApplicationService {
    constructor(app: ExpressDataApplication);
}