import { ApplicationService, ApplicationServiceConstructor } from '@themost/common';
import { DataContext } from '@themost/data';
import { ExpressDataApplication } from '@themost/express';
import { OneRosterProviderFilter } from './models/OneRosterBaseType';

export declare interface OneRosterSyncEvent {
    filter: OneRosterProviderFilter;
    emitter: any;
}

export declare class OneRosterService extends ApplicationService {
    constructor(app: ExpressDataApplication);

    useStrategy(serviceCtor: ApplicationServiceConstructor<any>, strategyCtor: ApplicationServiceConstructor<any>): this;
    useService(serviceCtor: ApplicationServiceConstructor<any>): this;
    hasService<T>(serviceCtor: ApplicationServiceConstructor<T>): boolean;
    getService<T>(serviceCtor: ApplicationServiceConstructor<T>): T;

    sync(context: DataContext, event?: OneRosterSyncEvent, options?: { mode: 'full' | 'partial' }): Promise<[string, any[]][]>;

}