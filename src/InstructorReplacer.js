import { ApplicationService } from '@themost/common';
import { instructorRouter } from './routes/instructorRouter';
import { Router } from 'express';
class InstructorReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // do nothing
    const app = this.getApplication();
    if (app && app.serviceRouter) {
      app.serviceRouter.subscribe( serviceRouter => {
        // create new router
        const addRouter = Router();
        addRouter.use('/instructors', instructorRouter(app));
        // insert router at the beginning of serviceRouter.stack
        serviceRouter.stack.unshift.apply(serviceRouter.stack, addRouter.stack);
      });
    }
  }

}

export {
  InstructorReplacer
}
