import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';

class CourseClassReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
    // get model definition
    const model = schemaLoader.getModelDefinition('CourseClass');
    const findAttribute = model.fields.find((field) => {
      return field.name === 'oneRoster'
    });
    if (findAttribute == null) {
      model.fields.push({
        "name": "oneRoster",
        "type": "OneRosterClassLink",
        "many": true,
        "nested": true,
        "multiplicity": "ZeroOrOne",
        "mapping": {
            "associationType": "association",
            "cascade": "delete",
            "parentModel": "CourseClass",
            "parentField": "id",
            "childModel": "OneRosterClassLink",
            "childField": "courseClass",
        }
      });
    }
    model.eventListeners = model.eventListeners || [];
    let found = model.eventListeners.find((item) => item.type === '@themost/data/previous-state-listener');
    if (found == null) {
      model.eventListeners.unshift({
        "type": "@themost/data/previous-state-listener"
      });
    }
    const type = path.resolve(__dirname, './listeners/AfterCourseClass');
    found = model.eventListeners.find((item) => item.type === type);
    if (found == null) {
      model.eventListeners.push({
        type
      });
    }
    schemaLoader.setModelDefinition(model);
  }

}

export {
    CourseClassReplacer
}
