import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';

export declare class CourseClassInstructorReplacer extends ApplicationService {
    constructor(app: ExpressDataApplication);
}