import { ApplicationService } from '@themost/common';
class DisableOneRosterReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }
  apply() {
    // do nothing
  }
}

module.exports = {
    DisableOneRosterReplacer
}
