import { DataEventArgs } from '@themost/data';

export declare class AfterOneRosterClassListener {
    afterSave(event: DataEventArgs, callback: (err?: Error) => void): void;
    afterSaveAsync(event: DataEventArgs): Promise<void>;
    afterRemove(event: DataEventArgs, callback: (err?: Error) => void): void;
}