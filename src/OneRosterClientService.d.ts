import { boolean } from 'mathjs';

export declare class OneRosterClientService {
    async getAllStudents(): Promise<any[]>;
    async getStudent(sourcedId: string): Promise<any>;
    async getStudentByUsername(username: string): Promise<any>;

    async getAllTeachers(): Promise<any[]>;
    async getTeacher(sourcedId: string): Promise<any>;

    async getResultsForStudent(): Promise<any[]>;
    async getClassesForStudent(sourcedId: string): Promise<any[]>;

    async getAllGradingPeriods(): Promise<any[]>;
    async getGradingPeriod(sourcedId: string): Promise<any>;

    async getLineItem(sourcedId: string): Promise<any>;

    async getClass(sourcedId: string): Promise<any>;

    async getSchool(sourcedId: string): Promise<any>;

    async getCourse(sourcedId: string): Promise<any>;

    async getAcademicSession(sourcedId: string): Promise<any>;

    async authorize(tokenURL: string, params: {grant_type: string, client_id: string, client_secret: string, scope: string}, force?: boolean)
        : Promise<{ access_token: string, refresh_token: string }>;
}