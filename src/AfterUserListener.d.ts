import { DataEventArgs } from '@themost/data';

export declare class AfterOneRosterClassListener {
    afterSaveAsync(event: DataEventArgs): Promise<void>;
    afterSave(event: DataEventArgs, callback: (err?: Error) => void): void;
    afterRemoveAsync(event: DataEventArgs): Promise<void>;
    afterRemove(event: DataEventArgs, callback: (err?: Error) => void): void;
}