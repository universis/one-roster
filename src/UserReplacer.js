import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';

class UserReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
    // get model definition
    [
      'User',
      'UserReference'
    ].forEach((name) => {
      const model = schemaLoader.getModelDefinition(name);
      if (model == null) {
        return;
      }
      model.eventListeners = model.eventListeners || [];
      let found = model.eventListeners.find((item) => item.type === '@themost/data/previous-state-listener');
      if (found == null) {
        model.eventListeners.unshift({
          "type": "@themost/data/previous-state-listener"
        });
      }
      const type = path.resolve(__dirname, './listeners/AfterUser');
      found = model.eventListeners.find((item) => item.type === type);
      if (found == null) {
        model.eventListeners.push({
          type
        });
      }
      schemaLoader.setModelDefinition(model);
    });

  }

}

export {
    UserReplacer
}
