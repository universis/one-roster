import { OpenDataParser, IdentifierToken, SyntaxToken, LiteralToken, SqlUtils } from '@themost/query';
class OneRosterFilterParser extends OpenDataParser {
    constructor() {
        super();
    }

    toList() {
        if (typeof this.source !== 'string')
        return [];
        this.current = 0;
        this.offset = 0;
        const result = [];
        let token = this.getNext();
        while (token)
        {
            result.push(token);
            token = this.getNext();
            if (token && token.type === 'Identifier' && token.identifier === 'contains') {
                // get next token
                // expected a string literal token
                const nextToken = this.getNext();
                // remove last token
                const lastToken = result.splice(result.length - 1, 1)[0];
                result.push(
                    new IdentifierToken('indexof'),
                    new SyntaxToken('('),
                    lastToken,
                    new SyntaxToken(','),
                    nextToken,
                    new SyntaxToken(')'),
                    new IdentifierToken('ge')
                );
                token = new LiteralToken(0);
            }
        }
        return result;
    }

    getString() {
        if (Array.isArray(this.tokens) === false) {
            return '';
        }
        let str = '';
        for (const token of this.tokens) {
            if (token.type === 'Identifier') {
                if (this.getOperator(token)) {
                    str += ' ';
                    str += token.identifier;
                    str += ' ';
                } else {
                    str += token.identifier;
                }
            } else if (token.type === 'Syntax') {
                str += token.syntax;
            } else if (token.type === 'Literal') {
                str += SqlUtils.escape(token.value)
            }
        }
        return str.trim();
    }

    getNext() {
        let _current = this.current;
        let _source = this.source;
        let _offset = this.offset;

        if (_offset >= _source.length)
            return null;

        while (_offset < _source.length && OpenDataParser.isWhitespace(_source.charAt(_offset)))
        {
            _offset++;
        }
        if (_offset >= _source.length)
            return null;
        _current = _offset;
        this.current = _current;
        let c = _source.charAt(_current);
        let c1;
        switch (c)
        {
            case '-':
                return this.parseSign();
            case '\'':
                return this.parseString();
            case '(':
            case ')':
            case ',':
            case '/':
                return this.parseSyntax();
            case '=':
                this.offset = this.current + 1;
                return new IdentifierToken('eq');
            case '>':
                c1 = _source.charAt(this.current + 1);
                if (c1 === '=') {
                    this.offset = this.current + 2;
                    return new IdentifierToken('ge');
                }
                this.offset = this.current + 1;
                return new IdentifierToken('gt');
            case '<':
                c1 = _source.charAt(this.current + 1);
                if (c1 === '=') {
                    this.offset = this.current + 2;
                    return new IdentifierToken('le');
                }
                this.offset = this.current + 1;
                return new IdentifierToken('lt');
            case '!':
                // expect !=
                c1 = _source.charAt(this.current + 1);
                if (c1 !== '=') {
                    throw new Error(`Unexpected operator "${c}" at offset ${this.offset}`);
                }
                this.offset = this.current + 2;
                return new IdentifierToken('ne');
            case '~':
                this.offset = this.current + 1;
                return new IdentifierToken('contains');
            default:
                if (c === '.' && OpenDataParser.isIdentifierStartChar(_source.charAt(this.current + 1))) {
                    this.offset = this.current + 1;
                    return new SyntaxToken('/');
                }
                else if (OpenDataParser.isDigit(c))
                {
                    return this.parseNumeric();
                }
                else if (OpenDataParser.isIdentifierStartChar(c))
                {
                    return this.parseIdentifier(false);
                }
                else
                {
                    throw new Error(`Unexpected character "${c}" at offset ${_current}`);
                }
        }
    }
}

export {
    OneRosterFilterParser
}