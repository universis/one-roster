import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';

class CourseReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
    // get model definition
    const model = schemaLoader.getModelDefinition('Course');
    const findAttribute = model.fields.find((field) => {
      return field.name === 'oneRoster'
    });
    if (findAttribute == null) {
      model.fields.push({
        "name": "oneRoster",
        "type": "OneRosterCourseLink",
        "many": true,
        "nested": true,
        "multiplicity": "ZeroOrOne",
        "mapping": {
            "associationType": "association",
            "cascade": "delete",
            "parentModel": "Course",
            "parentField": "id",
            "childModel": "OneRosterCourseLink",
            "childField": "course",
        }
      });
      schemaLoader.setModelDefinition(model);
    }
  }

}

export {
    CourseReplacer
}
