import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';

export declare class DepartmentReplacer extends ApplicationService {
    constructor(app: ExpressDataApplication);
}