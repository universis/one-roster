import { ExpressDataApplication } from '@themost/express';
import { Router } from 'express';
export function oneRosterRouter(app: ExpressDataApplication): Router;