import { ApplicationService, DataError, DataNotFoundError } from '@themost/common';
import { ModelClassLoaderStrategy, DataPermissionEventListener, SchemaLoaderStrategy, EdmMapping, EdmType, DataObject } from '@themost/data';
import {promisify} from 'util';
import { OneRosterClientService } from './OneRosterClientService';

class Student extends DataObject {
    @EdmMapping.func('OutgoingResults', EdmType.CollectionOf('Object'))
    async getOutgoingResults() {
        // validate permission
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.context.model('StudentCourse'), // model
            state: 0, // read
            target: {
                student: this.getId() // student
            }
        });
        const username = await this.getModel().where('id').equal(this.getId()).select('user/name').value();
        const results = await this.context.model('OneRosterResults').where('student/username').equal(username)
            .select(
                'sourcedId',
                'lineItem/title as lineItemTitle',
                'lineItem/assignDate as lineItemAssignDate',
                'lineItem/class/title as classTitle',
                'lineItem/class/classCode as classCode',
                'lineItem/class/school/name as schoolName',
                'lineItem/class/school/identifier as schoolIdentifier',
                'lineItem/category/title as lineItemCategory',
                'lineItem/gradingPeriod/title as gradingPeriodTitle',
                'lineItem/gradingPeriod/schoolYear as gradingPeriodSchoolYear',
                'scoreStatus',
                'score',
                'scoreDate',
                'metadata/scoreText as scoreText',
                'metadata/altScoreText as altScoreText',
                'metadata/passed as passed'
            )
            .silent().orderByDescending('lineItem/gradingPeriod/parent/title').getAllItems();
        return {
            skip: 0,
            total: results.length,
            value: results
        };
    }
    
    @EdmMapping.param('provider', EdmType.EdmString, false)
    @EdmMapping.action('IncomingResults', EdmType.CollectionOf('Object'))
    async getIncomingResults(provider) {
        // validate permission
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.context.model('StudentCourse'), // model
            state: 0, // read
            target: {
                student: this.getId() // student
            }
        });
        const serviceProvider = await this.context.model('ServiceProvider').where('alternateName').equal(provider).silent().getTypedItem();
        if (serviceProvider == null) {
            throw new DataNotFoundError('Service provider cannot be found', null, 'ServiceProvider', 'alternateName');
        }
        // get keychain item
        /**
         * @type {Array<*>}
         */
        const keychain = this.context.getApplication().getConfiguration().getSourceAt('settings/universis/keychain');
        if (Array.isArray(keychain) === false) {
            throw new DataError('E_CONFIG', 'Invalid application configuration. Service authenticator is not accessible.', null, 'ServiceProvider');
        }
        /**
         * @type {{ tokenURL: string, grant_type: string, client_id: string, client_secret: string, scope: string }|undefined}
         */
        const authenticator = keychain.find((item) => item.identifier === serviceProvider.authenticator);
        if (authenticator == null) {
            throw new DataError('E_CONFIG', 'Invalid application configuration. Service authenticator cannot be found.', null, 'ServiceProvider');
        }
        // get params
        const { grant_type, client_id, client_secret, scope } = authenticator
        const client = await new OneRosterClientService(serviceProvider.url).authorize(authenticator.tokenURL, {
            grant_type,
            client_id,
            client_secret,
            scope
        });

        const username = await this.property('user').select('name').value();
        if (username == null) {
            throw new DataError('E_NULL', 'Student user attribute cannot not be null at this context', null, 'Student', 'user');
        }
        /**
         * @type {{sourcedId: string, username: string}=}
         */
        const student = await client.getStudentByUsername(username);
        if (student == null) {
            return [];
        }
        const results = await client.getResultsForStudent(student.sourcedId);
        return results;
    }
}


class StudentReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('Student');
        // add listener
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const StudentClass = loader.resolve(model);
        // extend model
        StudentClass.prototype.getOutgoingResults = Student.prototype.getOutgoingResults;
        StudentClass.prototype.getIncomingResults = Student.prototype.getIncomingResults;
    }

}

export {
    StudentReplacer
}