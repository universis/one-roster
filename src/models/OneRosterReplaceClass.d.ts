import { ApplicationBase } from '@themost/common';
import { DataObject } from '@themost/data';
import { OneRosterProviderFilter } from './OneRosterBaseType';
import { OneRosterProvider } from './OneRosterProvider';

export interface OneRosterReplaceClassFilter extends OneRosterProviderFilter {
    classCode?: string;
}

export declare class OneRosterReplaceClass extends DataObject {
    //
}

export declare class OneRosterReplaceClassProvider extends OneRosterProvider {
    constructor(app: ApplicationBase);
    preSync(context: DataContext, filter: OneRosterReplaceClassFilter): Promise<any[][]>;
    sync(context: DataContext, filter: OneRosterReplaceClassFilter): Promise<void>;
}
