import { JsonLogger } from '@themost/json-logger';
import { WriteStream } from 'fs';

export declare class OneRosterLogger extends JsonLogger {
    constructor();
}

export declare class OneRosterFileLogger extends JsonLogger {
    constructor(outputStream: WriteStream);
}