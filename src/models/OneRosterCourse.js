import { EdmMapping } from '@themost/data';
import {OneRosterBaseType} from './OneRosterBaseType';
import '@themost/promise-sequence';
import { SqlFormatter } from '@themost/query';
import { OneRosterProvider } from './OneRosterProvider';
@EdmMapping.entityType('OneRosterCourse')
class OneRosterCourse extends OneRosterBaseType {
    /**
     * @type {string}
     */
    title;
    /**
     * @type {OneRosterAcademicSession}
     */
    schoolYear;
    /**
     * @type {string}
     */
    courseCode;
    /**
     * @type {OneRosterOrg}
     */
    org;
    /**
     * @type {Array<*>}
     */
    subjects;

    constructor() {
        super();
    }

}

class OneRosterCourseProvider extends OneRosterProvider {
    constructor(app) {
        super(app);
    }
    /**
         * @param {DataContext} context 
         * @param {import('./OneRosterCourse').OneRosterCourseProviderFilter} filter
         * @param {{force?: boolean, description?: string}=} options
         */
    async preSync(context, filter, options) {

        const { force } = Object.assign({}, { force: false }, options);
        const q = context.model('CourseClass').select(
            'id',
            'course/id as courseCode',
            'course/name as title',
            'course/department/id as department',
            'course/isEnabled as enabled',
            'course/dateModified as dateModified'
        );
        // get department filter
        if (filter.department) {
            q.where('course/department').equal(filter.department);
        }
        if (Object.prototype.hasOwnProperty.call(filter, 'courseCode')) {
            if (q.query.$where) {
                q.and('course').equal(filter.courseCode);
            } else {
                q.where('course').equal(filter.courseCode);
            }
        }

        if (Object.prototype.hasOwnProperty.call(filter, 'academicYear')) { 
            q.and('year').equal(filter.academicYear);
        }

        if (Object.prototype.hasOwnProperty.call(filter, 'academicPeriod')) { 
            q.and('period').equal(filter.academicPeriod);
        }
        
        if (q.query.$where == null) {
            throw new Error('Invalid filter');
        }
        const sqlFormatter = context.application.getService(SqlFormatter);
        if (sqlFormatter) {
            //this.logger.debug(context, 'OneRosterSyncAction', 'queryCourses', sqlFormatter.format(q.query));
        }

        const items = await q.expand('locales').silent().take(-1).getItems();
        // get default locale
        let defaultLocale = context.getConfiguration().getSourceAt('settings/i18n/defaultLocale');

        // filter existing courses by dateModified
        const queryOneRosterCourses = context.model('OneRosterCourse').select(
            'courseCode',
            'dateLastModified'
        ).where('dateLastModified').notEqual(null);
        if (filter.department) {
            queryOneRosterCourses.and('org/identifier').equal(`DEPT-${filter.department}`);
        }
        if (Object.prototype.hasOwnProperty.call(filter, 'courseCode')) {
            queryOneRosterCourses.and('courseCode').equal(filter.courseCode);
        }

        if (sqlFormatter) {
            this.logger.debug(context, 'OneRosterSyncAction', filter, 'queryOneRosterCourses', sqlFormatter.format(queryOneRosterCourses.query));
        }

        this.logger.info(context, 'OneRosterSyncAction', filter, 'Getting existing courses');
        const oneRosterCourses = await queryOneRosterCourses.silent().take(-1).getItems();
        this.logger.info(context, 'OneRosterSyncAction', filter, `Found ${items.length} courses`);

        const courses = items.filter((item) => {
            if (force) {
                return true;
            }
            // filter and exclude course classes based on date modified
            const existing = oneRosterCourses.find((x) => x.courseCode === item.courseCode.toString());
            if (existing && existing.dateLastModified >= item.dateModified) {
                return false;
            }
            return true;
        }).map((item) => {
            return {
                status: item.enabled ? 'active' : 'inactive',
                dateLastModified: item.dateModified || new Date(),
                title: item.title,
                courseCode: item.courseCode,
                org: {
                    identifier: `DEPT-${item.department}`
                },
                metadata: {
                    locales: item.locales.map(({inLanguage, name: title}) => {
                        return {
                            inLanguage,
                            title
                        };
                    })
                }
            }
        }).map((course) => {
            if (defaultLocale) {
                const find = course.metadata.locales.find((locale) => locale.inLanguage === defaultLocale);
                if (find == null) {
                    course.metadata.locales.push({
                        inLanguage: defaultLocale,
                        title: course.title
                    });
                }
            }
            return course
        });
        this.logger.info(context, 'OneRosterSyncAction', filter, `Returning ${courses.length} course(s) to insert or update`);
        return [
            [
                'OneRosterCourse',
                courses
            ]
        ];
    }

}

export {
    OneRosterCourse,
    OneRosterCourseProvider
};
