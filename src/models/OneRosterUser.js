import { DataObjectState, EdmMapping } from '@themost/data';
import { OneRosterBaseType } from './OneRosterBaseType';
import '@themost/promise-sequence';
import Chance from 'chance';
import { QueryEntity, QueryExpression, QueryField } from '@themost/query';
import { SqlFormatter } from '@themost/query';
import { OneRosterProvider } from './OneRosterProvider';
import { Guid } from '@themost/common';
import MD5 from 'crypto-js/md5';
import { template } from 'lodash';

if (typeof Guid.from !== 'function') {
    Guid.from = function(value) {
        const str = MD5(value).toString();
        return new Guid([
            str.substring(0, 8),
            str.substring(8, 12),
            str.substring(12, 16),
            str.substring(16, 20),
            str.substring(20, 32)
        ].join('-'));
    }
}

function randomPassword() {
    const chance = new Chance();
    return chance.string({
        length: 16,
        alpha: true,
        numeric: true,
        symbols: true
    })  + chance.string({
        length: 2,
        numeric: true
    }) + chance.string({
        length: 2,
        symbols: true
    });
}

@EdmMapping.entityType('OneRosterUser')
class OneRosterUser extends OneRosterBaseType {
    /**
     * @type {string}
     */
    username;
    constructor() {
        super();
    }

}

class OneRosterUserProvider extends OneRosterProvider {

    constructor(app) {
        super(app);
    }

    /**
     * @param {import('@themost/data').DataContext} context 
     * @param {{academicYear: number, academicPeriod: number, department?: string | number, classCode?: string | number }} filter 
     * @returns 
     */
    async preSync(context, filter) {

        /**
         * @type {import('@themost/data').DataQueryable}
         */
        const queryInstructors = context.model('CourseClassInstructors').select(
            'instructor/user/name as username',
            'instructor/user/enabled as enabledUser',
            'instructor/email as email',
            'instructor/familyName as familyName',
            'instructor/givenName as givenName',
            'instructor/middleName as middleName',
            'instructor/workPhone as phone',
            'instructor/department/id as department',
            'instructor/user/dateModified as dateLastModified',
            'courseClass/course/department as classDepartment'
        ).where('instructor/user').notEqual(null);
        if (filter.department) {
            queryInstructors.and('courseClass/course/department').equal(filter.department);
        }
        if (Object.prototype.hasOwnProperty.call(filter, 'classCode')) {
            // use classCode to filter instructors
            queryInstructors.and('courseClass').equal(filter.classCode);
        } else {
            // use the given academic year and period only if classCode is not provided
            queryInstructors.and('courseClass/year').equal(filter.academicYear)
            .and('courseClass/period').equal(filter.academicPeriod)
        }
        // use dateLastModified to filter existing users
        const { viewAdapter: OneRosterUsers } = context.model('OneRosterUser');
        const { viewAdapter: CourseClassInstructors } = context.model('CourseClassInstructors');
        // eslint-disable-next-line no-unused-vars
        const dateModified = new QueryField('dateModified').from('user');
        // eslint-disable-next-line no-unused-vars
        const dateLastModified = new QueryField('dateLastModified').from(OneRosterUsers)
        const sourcedId = new QueryField('sourcedId').from(OneRosterUsers).as('sourcedId');
        queryInstructors.query.distinct();
        queryInstructors.query.join(new QueryEntity(OneRosterUsers).left()).with(
            new QueryExpression().where(
                new QueryField('name').from('user')
            ).equal(
                new QueryField('username').from(OneRosterUsers)
            )
        ); //.prepare().where(dateLastModified).equal(null).or(dateModified).greaterThan(dateLastModified);

        const selectInstructor = queryInstructors.query.$select[CourseClassInstructors];
        // select dateLastModified from OneRosterUsers in order to check if user has been modified
        selectInstructor.push(new QueryField('dateLastModified').from(OneRosterUsers).as('oneRosterDateLastModified'));

        const selectArguments = [
            'student/user/name as username',
            'student/user/enabled as enabledUser',
            'student/person/email as email',
            'student/person/familyName as familyName',
            'student/person/givenName as givenName',
            'student/user/dateModified as dateLastModified',
            'student/department/id as department',
            'student/department/abbreviation as departmentAbbreviation',
            'student/department/alternativeCode as departmentAlternativeCode',
            'student/inscriptionYear/id as inscriptionYear',
            'student/inscriptionPeriod/id as inscriptionPeriod',
            'student/semester as semester',
            'courseClass/course/department as classDepartment',
            'student/studentIdentifier as studentIdentifier',
            'student/uniqueIdentifier as uniqueIdentifier'
        ];
        if (context.model('DepartmentConfiguration').getAttribute('domain')) {
            selectArguments.push('student/department/departmentConfiguration/domain as departmentDomain');
        }

        const queryStudents = context.model('StudentCourseClass').select(
            ...selectArguments
        ).where('student/user').notEqual(null)
            .and('student/studentStatus/alternateName').equal('active'); // only active students
        if (filter.department) {
            queryStudents.and('courseClass/course/department').equal(filter.department);
        }
        if (Object.prototype.hasOwnProperty.call(filter, 'classCode')) {
            // use classCode to filter students (omit academicYear and academicPeriod)
            queryStudents.and('courseClass').equal(filter.classCode);
        } else {
            // use the given academic year and period only if classCode is not provided
            queryStudents.and('courseClass/year').equal(filter.academicYear).and('courseClass/period').equal(filter.academicPeriod)
        }
        queryStudents.query.distinct();
        // use dateLastModified to filter existing users
        queryStudents.query.join(new QueryEntity(OneRosterUsers).left()).with(
            new QueryExpression().where(
                new QueryField('name').from('user')
            ).equal(
                new QueryField('username').from(OneRosterUsers)
            )
        ); //.prepare().where(dateLastModified).equal(null).or(dateModified).greaterThan(dateLastModified);
        
        const { viewAdapter: StudentCourseClasses } = context.model('StudentCourseClass');
        const selectStudent = queryStudents.query.$select[StudentCourseClasses];
        selectStudent.push(sourcedId);
        // select dateLastModified from OneRosterUsers in order to check if user has been modified
        selectStudent.push(new QueryField('dateLastModified').from(OneRosterUsers).as('oneRosterDateLastModified'));

        const queryOrgs = context.model('OneRosterOrg').select('sourcedId', 'identifier');

        const sqlFormatter = context.application.getService(SqlFormatter);
        if (sqlFormatter) {
            // this.logger.debug(context, 'OneRosterSyncAction', filter, 'queryInstructors', sqlFormatter.format(queryInstructors.query));
            // this.logger.debug(context, 'OneRosterSyncAction', filter, 'queryStudents', sqlFormatter.format(queryCourseClasses.query));
        }
        this.logger.info(context, 'OneRosterSyncAction', filter, 'Getting instructors and students');
        const results = await Promise.sequence([
            () => queryInstructors.silent().take(-1).getItems(),
            () => queryStudents.silent().take(-1).getItems(),
            () => queryOrgs.silent().take(-1).getItems()
        ]);
        this.logger.info(context, 'OneRosterSyncAction', filter, `Found ${results[0].length} instructors`);
        this.logger.info(context, 'OneRosterSyncAction', filter, `Found ${results[1].length} students`);
        // append instructors as users
        const users = [];
        const [instructors, students, orgs] = results;

        const filterKnownUser = (item) => {
            // filter for inserting users with GUID as username
            if (Guid.isGuid(item.username) && item.sourcedId == null) {
                return false;
            }
            return true;
        }

        const filterModifiedUser = (item) => {
            if (item.oneRosterDateLastModified == null)  { return true; }
            if (item.dateLastModified > item.oneRosterDateLastModified) { return true; }
            return false;
        }

        instructors.filter(filterModifiedUser).filter(filterKnownUser).forEach((instructor) => {
            const findUser = users.find((x) => x.username === instructor.username);
            if (findUser) {
                const findIndex = findUser.orgs.findIndex((org) => org.identifier === `DEPT-${instructor.classDepartment}`);
                if (findIndex < 0) {
                    const findOrg = orgs.find((org) => org.identifier === `DEPT-${instructor.classDepartment}`);
                    if (findOrg == null) {
                        throw new Error(`Organization with identifier 'DEPT-${instructor.classDepartment}' not found.`);
                    }
                    findUser.orgs.push({
                        sourcedId: findOrg.sourcedId
                    });
                }
                return;
            }
            const findInstructorOrg = orgs.find((org) => org.identifier === `DEPT-${instructor.department}`);
            if (findInstructorOrg == null) {
                throw new Error(`Organization with identifier 'DEPT-${instructor.department}' not found.`);
            }

            const res = {
                role: 'teacher',
                status: instructor.enabledUser ? 'active' : 'inactive',
                dateLastModified: instructor.dateLastModified,
                username: instructor.username,
                identifier: instructor.username,
                enabledUser: instructor.enabledUser,
                email: instructor.email,
                familyName: instructor.familyName,
                middleName: instructor.middleName,
                givenName: instructor.givenName,
                password: randomPassword(),
                orgs: [
                    {
                        sourcedId: findInstructorOrg.sourcedId
                    }
                ]
            }
            // if instructor has already been inserted
            if (instructor.sourcedId) {
                // assign sourcedId
                Object.assign(res, {
                    sourcedId: instructor.sourcedId
                });
            }
            // validate if user has a GUID as username
            if (Guid.isGuid(res.username)) {
                // mark user for deletion
                Object.assign(res, {
                    $state: DataObjectState.Delete
                })
            }
            users.push(res);
        });
        const matriculationNumberFormat = context.getConfiguration().getSourceAt('settings/universis/one-roster/matriculationNumberFormat') || '${uniqueIdentifier}';
        const alternateMatriculationNumberFormat = context.getConfiguration().getSourceAt('settings/universis/one-roster/alternateMatriculationNumberFormat') || '${department}/${studentIdentifier}';
        const compiledMatriculationNumber = template(matriculationNumberFormat);
        const compiledAlternateMatriculationNumber = template(alternateMatriculationNumberFormat);
        // append students as users
        students.filter(filterModifiedUser).filter(filterKnownUser).forEach((student) => {
            const findUser = users.find((x) => x.username === student.username);
            if (findUser) {
                const findIndex = findUser.orgs.findIndex((org) => org.identifier === `DEPT-${student.classDepartment}`);
                if (findIndex < 0) {
                    // get org sourcedId
                    const findOrg = orgs.find((org) => org.identifier === `DEPT-${student.classDepartment}`);
                    if (findOrg == null) {
                        throw new Error(`Organization with identifier 'DEPT-${student.classDepartment}' not found.`);
                    }
                    findUser.orgs.push({
                        sourcedId: findOrg.sourcedId,
                    });
                }
                return;
            }
            const findStudentOrg = orgs.find((org) => org.identifier === `DEPT-${student.department}`);
            if (findStudentOrg == null) {
                throw new Error(`Organization with identifier 'DEPT-${student.department}' not found.`);
            }
            const res = {
                role: 'student',
                status: student.enabledUser ? 'active' : 'inactive',
                dateLastModified: student.dateLastModified,
                username: student.username,
                identifier: student.username,
                enabledUser: student.enabledUser,
                email: student.email,
                familyName: student.familyName,
                givenName: student.givenName,
                password: randomPassword(),
                orgs: [
                    {
                        sourcedId: findStudentOrg.sourcedId
                    }
                ],
                metadata: {
                    matriculationNumber: compiledMatriculationNumber(student),
                    alternateMatriculationNumber: compiledAlternateMatriculationNumber(student)
                }
            };
            if (student.sourcedId) {
                Object.assign(res, {
                    sourcedId: student.sourcedId
                });
            }
            if (Guid.isGuid(res.username)) {
                // set user as deleted
                Object.assign(res, {
                    $state: DataObjectState.Delete
                })
            }
            users.push(res);
        });
        this.logger.info(context, 'OneRosterSyncAction', filter, `Returning ${users.length} user(s) to insert or update`);
        return [
            [
                'OneRosterUser',
                users
            ]
        ]
    }

    /**
     * @param {import('@themost/data').DataContext} context 
     * @param {{academicYear: number, academicPeriod: number, department: string | number, classCode?: string | number }} filter 
     * @returns 
     */
    async sync(context, filter) {
        // migrate OneRosterUser
        const Users = context.model('OneRosterUser');
        const { sourceAdapter: OneRosterUser } = Users;
        const results = await this.preSync(context, filter);
        const [,users] = results[0];
        if (users.length === 0) {
            this.logger.info(context, 'OneRosterSyncAction', filter, 'Finished users\'s synchronization without getting any user');
        } else {
            let index = 0;
            let total = users.length;
            const step = total > 1000 ? 100 : total > 100 ? 50 : 10;
            
            const baseType = 'user';
            // a light-weigth procedure of inserting or updating users
            const Users = context.model('OneRosterUser');
            // get baseHref
            const baseHref = Users.context.getConfiguration().getSourceAt('settings/universis/one-roster/origin') || '/ims/oneroster/v1p1/';
            for (const user of users) {
                index++;
                if (index % step === 0) {
                    this.logger.log(context, 'OneRosterSyncAction', filter, `(OneRosterUser) Updating or inserting ${index} of ${total} item(s)`);
                }
                const findUser = await Users.where('username').equal(user.username).silent().flatten().getItem();
                // get user orgs
                const orgs = user.orgs;
                let sourcedId;
                delete user.orgs;
                if (findUser) {
                    // get sourcedId
                    sourcedId = findUser.sourcedId;
                    // assign data
                    Object.assign(findUser, user);
                    // check if user has not been marked for deletion (preSync operation may set $state to DataObjectState.Delete)  
                    if (user.$state === DataObjectState.Delete) {
                        // if so, delete user
                        await context.db.executeAsync(new QueryExpression().delete(OneRosterUser).where('username').equal(user.username));
                    } else {
                        // otherwise, cast user for update
                        const data = Users.cast(user, DataObjectState.Update);
                        await context.db.executeAsync(new QueryExpression().update(OneRosterUser).set(data).where('username').equal(user.username)); 
                    }
                } else {
                    // create user
                    // get sourcedId
                    sourcedId = Guid.from(user.username).toString();
                    // create user.href
                    const href =  baseHref + Users.collectionName + '/' + sourcedId;
                    Object.assign(user, {
                        sourcedId,
                        baseType,
                        href
                    });
                    // check if user has not been marked for deletion (preSync operation may set $state to DataObjectState.Delete)  
                    if (user.$state !== DataObjectState.Delete) {
                        // insert user
                        const data = Users.cast(user, DataObjectState.Insert);
                        await context.db.executeAsync(new QueryExpression().insert(data).into(OneRosterUser)); 
                    }
                }
                // get user orgs
                const userOrgs = orgs.map((org) => {
                    return {
                        object: sourcedId,
                        value: org.sourcedId
                    }
                });
                if (findUser) {
                    // delete existing user orgs
                    await context.db.executeAsync(new QueryExpression().delete('OneRosterUserOrgs').where('object').equal(sourcedId));
                }
                // check if user has not been marked for deletion (preSync operation may set $state to DataObjectState.Delete)  
                if (user.$state !== DataObjectState.Delete) {
                    // insert user orgs
                    for (const userOrg of userOrgs) {
                        await context.db.executeAsync(new QueryExpression().insert(userOrg).into('OneRosterUserOrgs'));
                    }
                }
            }
            this.logger.log(context, 'OneRosterSyncAction', filter, `(OneRosterUser) Finished sychronizing ${total} item(s)`);
        }
    }
}

class OneRosterNoUserProvider extends OneRosterProvider {
    constructor(app) {
        super(app);
    }
    // eslint-disable-next-line no-unused-vars
    async preSync(context, filter) {
        return [
            [
                'OneRosterUser',
                []
            ]
        ];
    }
}

export {
    OneRosterUser,
    OneRosterUserProvider,
    OneRosterNoUserProvider
};
