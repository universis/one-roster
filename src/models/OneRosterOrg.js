import { EdmMapping } from '@themost/data';
import {OneRosterBaseType} from './OneRosterBaseType';
import '@themost/promise-sequence';
import { OneRosterProvider } from './OneRosterProvider';
@EdmMapping.entityType('OneRosterOrg')
class OneRosterOrg extends OneRosterBaseType {
    /**
     * @type {string}
     */
    name;
    /**
     * @type {string}
     */
    type;
    /**
     * @type {string}
     */
    identifier;
    /**
     * @type {OneRosterOrg}
     */
    parent;
    /**
     * @type {Array<OneRosterOrg>}
     */
    children;
    constructor() {
        super();
    }

}

class OneRosterOrgProvider extends OneRosterProvider {
    constructor(app) {
        super(app)
    }

    /**
     * @param {DataContext} context 
     * @param {OneRosterProviderFilter} filter
     */
     // eslint-disable-next-line no-unused-vars
     async preSync(context, filter) {
        this.logger.info(context, 'OneRosterSyncAction', filter, 'Getting local institute and departments');

        /**
         * @type {import('@themost/data').DataQueryable}
         */
        const queryDepartments = context.model('LocalDepartment').select(
            'id',
            'name',
            'organization',
            'dateModified'
        );
        this.logger.info(context, 'OneRosterSyncAction', filter, 'Getting local institute and department(s)');
        const results = await Promise.sequence([
            () => context.model('Institute').where('local').equal(true).select(
                'id', 'name', 'instituteConfiguration/dateModified as dateModified'
            ).take(-1).silent().getItems(),
            () => queryDepartments.silent().take(-1).getItems(),
            () => context.model('OneRosterOrg').silent().take(-1).getItems(),
        ]);
        this.logger.info(context, 'OneRosterSyncAction', filter, `Found ${results[0].length} institute and ${results[1].length} department(s)`);
        const institutes = results[0].map((item) => {
            return {
                status: 'active',
                dateLastModified: item.dateModified || new Date(),
                name: item.name,
                type: 'national',
                identifier: `INST-${item.id}`,
                parent: null
            }
        });
        const departments = results[1].filter((item) => {
            if (item.dateModified == null) {
                return true;
            }
            // find the corresponding org
            const org = results[2].find((org) => {
                return org.identifier === `DEPT-${item.id}`;
            });
            if (org == null) {
                return true;
            }
            return org.dateLastModified < item.dateModified;
        }).map((item) => {
            return {
                status: 'active',
                dateLastModified: item.dateModified || new Date(),
                name: item.name,
                type: 'school',
                identifier: `DEPT-${item.id}`,
                parent: {
                    identifier: `INST-${item.organization}`
                }
            }
        });
        const orgs = [].concat(institutes, departments);
        this.logger.info(context, 'OneRosterSyncAction', filter, `Returning ${orgs.length} org(s)`);
        return [
            [
                'OneRosterOrg',
                orgs
            ]
        ];
    }

}

export  {
    OneRosterOrg,
    OneRosterOrgProvider
};
