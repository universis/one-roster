import { ApplicationService, ApplicationBase } from '@themost/common';
import { DataObject } from '@themost/data';
import { OneRosterLineItem } from './OneRosterLineItem';
import { OneRosterAcademicSession } from './OneRosterAcademicSession';
import { OneRosterBaseType } from './OneRosterBaseType';
import { OneRosterProviderFilter } from './OneRosterBaseType';
import { OneRosterClass } from './OneRosterClass';
import { OneRosterLineItemCategory } from './OneRosterLineItemCategory';
import { OneRosterUser } from './OneRosterUser';
import { OneRosterProvider } from './OneRosterProvider';

export declare class OneRosterResult extends OneRosterBaseType {
    lineItem: OneRosterLineItem | string;
    student: OneRosterUser | string;
    scoreStatus: { id: string, name: string } | string;
    score?: number;
    scoreDate?: string;
    comment?: string;
}

export declare class OneRosterResultProvider extends OneRosterProvider {
    constructor(app: ApplicationBase)
    preSync(context: DataContext, filter: OneRosterProviderFilter): Promise<any[][]>;
}

export declare class OneRosterNoResultProvider extends OneRosterProvider {
    constructor(app: ApplicationBase)
    preSync(context: DataContext, filter: OneRosterProviderFilter): Promise<any[][]>;
}