import { ApplicationService } from '@themost/common';
import { OneRosterLogger } from './OneRosterLogger';
import { AsyncSeriesEventEmitter } from '@themost/events';

class OneRosterProvider extends ApplicationService {
    constructor(app) {
        super(app);
        this.logger = new OneRosterLogger();
        /**
         * @type {AsyncSeriesEventEmitter<{target: OneRosterProvider, entityType: string, items: Array<*>}>}
         */
        this.filtering = new AsyncSeriesEventEmitter();
    }
}

export {
    OneRosterProvider
}