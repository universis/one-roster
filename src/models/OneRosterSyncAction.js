import {DataObject, EdmMapping} from '@themost/data';
import { TraceUtils } from '@themost/common';
import { OneRosterLogger } from './OneRosterLogger';

@EdmMapping.entityType('OneRosterSyncAction')
class OneRosterSyncAction extends DataObject {

    constructor() {
        super();
        /**
         * @type {OneRosterLogger|null}
         */
        let logger = null;
        const self = this;
        Object.defineProperty(this, 'logger', {
            configurable: true,
            enumerable: false,
            get: function() {
                if (logger == null) {
                    logger = new OneRosterLogger(self.context)
                }
                return logger;
            }
        });
    }

    async writeLog(level, message) {

        let eventType = 1
        switch(level) {
            case 'info':
                eventType = 1;
                break;
            case 'error':
                eventType = 4;
                break;
            case 'warning':
                eventType = 2;
                break;
        }

        const item = {
            title: message,
            eventSource: 'OneRoster',
            eventApplication: 'Universis',
            username: 'system',
            eventType: eventType
        }
        return new Promise((resolve, reject) => {
            void this.context.db.executeInTransaction((callback) => {
                return this.context.model('ActionEventLog').silent().insert(item).then(() => {
                    return this.getLogs().silent().insert({
                        id: item.id
                    }).then(() => {
                        return callback();
                    });
                }).catch((error) => {
                    TraceUtils.error(error);
                    return callback();
                })
            }, (error) => {
                if (error) {
                    return reject(error)
                }
                return resolve();
            });
        });
    }

    get logger() {
        return new OneRosterLogger(this.context);
    }

    async log(message) {
        return this.logger.log('OneRosterSyncAction', message);
    }


    async error(message) {
        return this.logger.error('OneRosterSyncAction', message);
    }

}

export {
    OneRosterSyncAction
}