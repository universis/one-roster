import { DataObject } from '@themost/data';


class OneRosterBaseType extends DataObject {
    /**
     * @type {string}
     */
    sourcedId;
    /**
     * @type {string}
     */
    status;
    /**
     * @type {Date}
     */
    dateLastModified;
    /**
     * @type {*}
     */
    metadata;
    constructor() { 
        super();
    }
}

export {
    OneRosterBaseType
};