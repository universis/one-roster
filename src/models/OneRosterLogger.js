import { Args } from '@themost/common';
import { DataContext } from '@themost/data';
import { ServerSentEventService } from '@universis/sse';

const { JsonLogger } = require('@themost/json-logger');

class InvalidContextArgument extends Error {
    constructor() {
        super('Expected an instance of DataContext');
    }
}
/**
 * 
 * @param {DataContext} context 
 * @param {Array<*>} args 
 */
function emit(context, args) {
    /**
     * @type {ServerSentEventService}
     */
    const service =  context.application.getService(ServerSentEventService);
    if (service == null) {
        return;
    }
    if (context.user == null) {
        return;
    }
    const { name: user, authenticationScope: scope } = context.user;
    if (user == null) {
        return;
    }
    if (user === 'anonymous') {
        return;
    }
    const [additionalType] = args;
    service.emit(user, scope, {
        additionalType: additionalType,
        message: args,
    });
}

class OneRosterFileLogger extends JsonLogger {
    /**
     * @param {import('fs').WriteStream} outputStream 
     */
    constructor(outputStream) {
        super({
            level: process.env.ONE_ROSTER_LOG_LEVEL || 'info'
        });
        this.outputStream = outputStream;
    }

    log() {
        return this.info.apply(this, arguments);
    }
    
    warn() {
        // get first argument that should be a data context
        const args = Array.from(arguments);
        // remove the first argument which is the current context
        args.shift();
        args.unshift(new Date());
        if (this.level >= JsonLogger.Levels.warn) {
            void this.outputStream.write(this.outputStream, JSON.stringify(args) + '\n', (err) => {
                if (err) {
                    return super.error.apply(this, arguments);
                }
            });
        }
    }
    error() {
        const args = Array.from(arguments);
        // remove the first argument which is the current context
        args.shift();
        args.unshift(new Date());
        if (this.level >= JsonLogger.Levels.error) {
            void this.outputStream.write(JSON.stringify(args) + '\n', (err) => {
                if (err) {
                    return super.error.apply(this, arguments);
                }
            });
        }
    }
    info() {
        const args = Array.from(arguments);
        // remove the first argument which is the current context
        args.shift();
        args.unshift(new Date());
        if (this.level >= JsonLogger.Levels.info) {
            void this.outputStream.write(JSON.stringify(args) + '\n', (err) => {
                if (err) {
                    return super.error.apply(this, arguments);
                }
            });
        }
    }
    debug() {
        const args = Array.from(arguments);
        // remove the first argument which is the current context
        args.shift();
        args.unshift(new Date());
        if (this.level >= JsonLogger.Levels.debug) {
            void this.outputStream.write(JSON.stringify(args) + '\n', (err) => {
                if (err) {
                    return super.error.apply(this, arguments);
                }
            });
        }
    }
    verbose() {
        const args = Array.from(arguments);
        // remove the first argument which is the current context
        args.shift();
        args.unshift(new Date());
        if (this.level >= JsonLogger.Levels.verbose) {
            void this.outputStream.write(JSON.stringify(args) + '\n', (err) => {
                if (err) {
                    return super.error.apply(this, arguments);
                }
            });
        }
    }


}

class OneRosterLogger extends JsonLogger {
    constructor() {
        super({
            level: process.env.ONE_ROSTER_LOG_LEVEL || 'info'
        })
    }

    log() {
        return this.info.apply(this, arguments);
    }

    warn() {
        // get first argument that should be a data context
        const args = Array.from(arguments);
        const context = args.shift();
        Args.check(context instanceof DataContext, new InvalidContextArgument());
        if (this.level >= JsonLogger.Levels.warn) {
            emit(context, args);
        }
    }
    error() {
        const args = Array.from(arguments);
        const context = args.shift();
        Args.check(context instanceof DataContext, new InvalidContextArgument());
        if (this.level >= JsonLogger.Levels.error) {
            emit(context, args);
        }
        return super.error.apply(this, arguments);
    }
    info() {
        const args = Array.from(arguments);
        const context = args.shift();
        Args.check(context instanceof DataContext, new InvalidContextArgument());
        if (this.level >= JsonLogger.Levels.info) {
            emit(context, args);
        }
    }
    debug() {
        const args = Array.from(arguments);
        const context = args.shift();
        Args.check(context instanceof DataContext, new InvalidContextArgument());
        if (this.level >= JsonLogger.Levels.debug) {
            emit(context, args);
        }
    }
    verbose() {
        const args = Array.from(arguments);
        const context = args.shift();
        Args.check(context instanceof DataContext, new InvalidContextArgument());
        if (this.level >= JsonLogger.Levels.verbose) {
            emit(context, args);
        }
    }


}

export {
    OneRosterLogger,
    OneRosterFileLogger
}