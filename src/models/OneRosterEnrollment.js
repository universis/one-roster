import { DataObjectState, EdmMapping } from '@themost/data';
import {OneRosterBaseType} from './OneRosterBaseType';
import '@themost/promise-sequence';
import { QueryEntity, QueryExpression, QueryField } from '@themost/query';
import { OneRosterAcademicSession } from './OneRosterAcademicSession';
import { v4 as uuidv4 } from 'uuid';
import { SqlFormatter } from '@themost/query';
import { OneRosterProvider } from './OneRosterProvider';
@EdmMapping.entityType('OneRosterEnrollment')
class OneRosterEnrollment extends OneRosterBaseType {
    /**
     * @type {OneRosterUser}
     */
    user;
    /**
     * @type {OneRosterClass}
     */
    class;
    /**
     * @type {OneRosterOrg}
     */
    school;
    /**
     * @type {OneRosterRoleType}
     */
    type;
    /**
     * @type {boolean}
     */
    primary;
     /**
     * @type {DateTime}
     */
    beginDate;
    /**
     * @type {DateTime}
     */
    endDate;
    constructor() {
        super();
    }

}

class OneRosterEnrollmentProvider extends OneRosterProvider {

    constructor(app) {
        super(app);
    }

    /**
     * @param {import('@themost/data').DataContext} context 
     * @param {{academicYear: number, academicPeriod: number, department: string | number, classCode?: string | number }} filter 
     * @returns 
     */
    async preSync(context, filter) {
        const queryInstructors = context.model('CourseClassInstructors').select(
            'courseClass/id as classCode',
            'courseClass/course/department/id as classDepartment',
            'instructor/user/name as username',
            'dateModified as dateLastModified'
        ).where('courseClass/year').equal(filter.academicYear)
            .and('courseClass/period').equal(filter.academicPeriod)
            .and('courseClass/department').equal(filter.department)
            .and('instructor/user').notEqual(null);

        if (filter.classCode) {
            queryInstructors.and('courseClass').equal(filter.classCode);
        }

        const queryStudents = context.model('StudentCourseClass').select(
            'courseClass/id as classCode',
            'courseClass/course/department/id as classDepartment',
            'courseClass/year/id as academicYear',
            'courseClass/period/id as academicPeriod',
            'student/user/name as username',
            'dateModified as dateLastModified'
        ).where('courseClass/year').equal(filter.academicYear)
            .and('courseClass/period').equal(filter.academicPeriod)
            .and('courseClass/department').equal(filter.department)
            .and('student/user').notEqual(null)
            .and('student/studentStatus/alternateName').equal('active'); // only active students
        
        if (filter.classCode) {
            queryStudents.and('courseClass').equal(filter.classCode);
        }

        const sqlFormatter = context.application.getService(SqlFormatter);
        if (sqlFormatter) {
            // this.logger.debug(context, 'OneRosterSyncAction', filter, 'queryInstructors' , sqlFormatter.format(queryInstructors.query)); 
            // this.logger.debug(context, 'OneRosterSyncAction', filter, 'queryStudents' , sqlFormatter.format(queryStudents.query)); 
        }

        this.logger.info(context, 'OneRosterSyncAction', filter, 'Getting instructors and students');
        const results = await Promise.sequence([
            () => queryInstructors.silent().take(-1).getItems(),
            () => queryStudents.silent().take(-1).getItems(),
        ]);
        this.logger.info(context, 'OneRosterSyncAction', filter, `Found ${results[0].length} instructor(s) and ${results[1].length} student(s)`);
        const instructorEnrollments = results[0].map((instructor) => {
            return {
                user: {
                    username: instructor.username
                },
                class: {
                    classCode: instructor.classCode.toString()
                },
                school: {
                    identifier: `DEPT-${instructor.classDepartment}`
                },
                type: 'teacher',
                role: 'teacher',
                status: 'active',
                primary: true,
                beginDate: null,
                endDate: null
            }
        });

        const studentEnrollments = results[1].map((student) => {
            return {
                user: {
                    username: student.username
                },
                class: {
                    classCode: student.classCode.toString()
                },
                school: {
                    identifier: `DEPT-${student.classDepartment}`
                },
                type: 'student',
                status: 'active',
                role: 'student',
                primary: false,
                beginDate: null,
                endDate: null,
                metadata: {
                    term: OneRosterAcademicSession.generateId(student.academicYear, student.academicPeriod)
                }
            }
        });
        const enrollments = [].concat(instructorEnrollments, studentEnrollments);
        this.logger.info(context, 'OneRosterSyncAction', filter, `Returning ${enrollments.length} enrollment(s)`);
        return [
            [
                'OneRosterEnrollment',
                enrollments
            ]
        ]

    }
}

class OneRosterEnrollmentWithSectionsProvider extends OneRosterProvider {

    constructor(app) {
        super(app);
    }

    /**
     * @param {import('@themost/data').DataContext} context 
     * @param {{academicYear: number, academicPeriod: number, department: string | number, classCode?: string | number, classSection?: number }} filter 
     * @returns 
     */
    async preSync(context, filter) {

        // Get course class instructors (including those who are not assigned to a section)
        const queryInstructors = context.model('CourseClassInstructors').select(
            'courseClass/id as classCode',
            'courseClass/course/department/id as classDepartment',
            'instructor/user/name as username',
            'dateModified as dateLastModified'
        ).where('instructor/user').notEqual(null);

        if (filter.department) {
            queryInstructors.and('courseClass/department').equal(filter.department)
        }
        //  append class code filter
        if (filter.classCode) {
            queryInstructors.and('courseClass').equal(filter.classCode);
        } else {
            queryInstructors.and('courseClass/year').equal(filter.academicYear).and('courseClass/period').equal(filter.academicPeriod);            
        }

        const { viewAdapter: OneRosterUsers } = context.model('OneRosterUser');
        const { viewAdapter: OneRosterClasses } = context.model('OneRosterClass');
        const { viewAdapter: CourseClassInstructors } = context.model('CourseClassInstructors');
        const { viewAdapter: CourseClassSectionInstructors } = context.model('CourseClassSectionInstructors');
        
        queryInstructors.query.join(new QueryEntity(OneRosterUsers)).with(
            new QueryExpression().where(
                new QueryField('name').from('user')
            ).equal(
                new QueryField('username').from(OneRosterUsers)
            )
        ).join(new QueryEntity(OneRosterClasses)).with(
            new QueryExpression().where(
                new QueryField('id').from('courseClass')
            ).equal(
                new QueryField('classCode').from(OneRosterClasses)
            )
        );

        await context.model('OneRosterIndivisibleClass').migrateAsync();
        const { viewAdapter: OneRosterIndivisibleClasses } = context.model('OneRosterIndivisibleClass');
        queryInstructors.query.leftJoin(new QueryEntity(OneRosterIndivisibleClasses)).with(
            new QueryExpression().where(
                new QueryField('id').from('courseClass')
            ).equal(
                new QueryField('courseClass').from(OneRosterIndivisibleClasses)
            )
        );

        // add extra attributes for getting OneRoster object ids
        const selectInstructor = queryInstructors.query.$select[CourseClassInstructors];
        selectInstructor.push(new QueryField('sourcedId').from(OneRosterClasses).as('class'));
        selectInstructor.push(new QueryField('sourcedId').from(OneRosterUsers).as('user'));
        selectInstructor.push(new QueryField('school').from(OneRosterClasses).as('school'));
        selectInstructor.push(new QueryField('indivisible').from(OneRosterIndivisibleClasses).as('indivisible'));

        // Get course class students (including only those who are assigned to a section, if any)
        const queryStudents = context.model('StudentCourseClass').select(
            'courseClass/id as classCode',
            'courseClass/course/department/id as classDepartment',
            'courseClass/year/id as academicYear',
            'courseClass/period/id as academicPeriod',
            'student/user/name as username',
            'dateModified as dateLastModified',
            'section'
        ).where('student/user').notEqual(null)
            .and('student/studentStatus/alternateName').equal('active'); // only active students;

        if (filter.department) {
            queryStudents.and('courseClass/department').equal(filter.department)
        }
        //  append class code filter
        if (filter.classCode) {
            queryStudents.and('courseClass').equal(filter.classCode);
        } else {
            queryStudents.and('courseClass/year').equal(filter.academicYear).and('courseClass/period').equal(filter.academicPeriod);            
        }
        //  append class section filter
        if (filter.classSection) {
            queryStudents.and('section').equal(filter.classSection);
        }
        const { viewAdapter: StudentCourseClasses } = context.model('StudentCourseClass');
        queryStudents.query.join(new QueryEntity(OneRosterUsers)).with(
            new QueryExpression().where(
                new QueryField('name').from('user')
            ).equal(
                new QueryField('username').from(OneRosterUsers)
            )
        );
        if (filter.classSection) {
            queryStudents.query.join(new QueryEntity(OneRosterClasses)).with(
                new QueryExpression().where(
                    new QueryField('classCode').from(OneRosterClasses)
                ).equal(
                    new QueryField({
                        $value: {
                            $concat: [
                                new QueryField('id').from('courseClass'),
                                '-',
                                new QueryField('section').from(StudentCourseClasses)
                            ]
                        }
                    })
                )
            );
        } else {
            queryStudents.query.join(new QueryEntity(OneRosterClasses)).with(
                new QueryExpression().where(
                    new QueryField('id').from('courseClass')
                ).equal(
                    new QueryField('classCode').from(OneRosterClasses)
                )
            );
        }
        // add extra attributes for getting OneRoster object ids
        const selectStudent = queryStudents.query.$select[StudentCourseClasses];
        selectStudent.push(new QueryField('sourcedId').from(OneRosterClasses).as('class'));
        selectStudent.push(new QueryField('sourcedId').from(OneRosterUsers).as('user'));
        selectStudent.push(new QueryField('school').from(OneRosterClasses).as('school'));
        // get class sections
        const queryClassSections = context.model('CourseClassSection').select(
            'id',
            'name',
            'section',
            'courseClass/course/department/id as classDepartment',
            'courseClass as classCode'
            ).where('courseClass/year').equal(filter.academicYear).and('courseClass/period').equal(filter.academicPeriod);
        // set department filter
        if (filter.department) {
            queryClassSections.and('courseClass/course/department').equal(filter.department);
        }
        // set class code filter
        if (filter.classCode) {
            queryClassSections.and('courseClass').equal(filter.classCode);
        }
        // set class section filter
        if (filter.classSection) {
            queryClassSections.and('section').equal(filter.classSection);
        }
        // get instructors for class sections
        const queryClassSectionInstructors = context.model('CourseClassSectionInstructor').select(
            'id',
            'section/section as section',
            'section/courseClass as classCode',
            'section/courseClass/course/department as classDepartment',
            'instructor/user/name as username',
            ).where('section/courseClass/year').equal(filter.academicYear).and('section/courseClass/period').equal(filter.academicPeriod);
        if (filter.classSection) {
            queryClassSectionInstructors.and('section/section').equal(filter.classSection);
        }
        queryClassSectionInstructors.query.join(new QueryEntity(OneRosterUsers)).with(
            new QueryExpression().where(
                new QueryField('name').from('user')
            ).equal(
                new QueryField('username').from(OneRosterUsers)
            )
        ).join(new QueryEntity(OneRosterClasses)).with(
            new QueryExpression().where(
                new QueryField('classCode').from(OneRosterClasses)
            ).equal(
                new QueryField({
                    $value: {
                        $concat: [
                            new QueryField('id').from('courseClass'),
                            '-',
                            new QueryField('section').from('section')
                        ]
                    }
                })
            )
        );

        const selectClassSectionInstructors = queryClassSectionInstructors.query.$select[CourseClassSectionInstructors];
        selectClassSectionInstructors.push(new QueryField('sourcedId').from(OneRosterClasses).as('class'));
        selectClassSectionInstructors.push(new QueryField('sourcedId').from(OneRosterUsers).as('user'));
        selectClassSectionInstructors.push(new QueryField('school').from(OneRosterClasses).as('school'));

        // set department filter
        if (filter.department) {
            queryClassSectionInstructors.and('section/courseClass/course/department').equal(filter.department);
        }
        // set class code filter
        if (filter.classCode) {
            queryClassSectionInstructors.and('section/courseClass').equal(filter.classCode);
        }

        const sqlFormatter = context.application.getService(SqlFormatter);
        if (sqlFormatter) {
            // this.logger.info(context, 'OneRosterSyncAction', filter, 'queryInstructors' , sqlFormatter.format(queryInstructors.query)); 
            // this.logger.info(context, 'OneRosterSyncAction', filter, 'queryStudents' , sqlFormatter.format(queryStudents.query));
            // this.logger.info(context, 'OneRosterSyncAction', filter, 'queryClassSections' , sqlFormatter.format(queryClassSections.query));
            // this.logger.info(context, 'OneRosterSyncAction', filter, 'queryClassSectionInstructors' , sqlFormatter.format(queryClassSectionInstructors.query));
        }
        this.logger.info(context, 'OneRosterSyncAction', filter, 'Getting instructors, students, class sections and class section instructors');
        const results = await Promise.sequence([
            () => queryInstructors.silent().take(-1).getItems(),
            () => queryStudents.silent().take(-1).getItems(),
            () => queryClassSections.silent().take(-1).getItems(),
            () => queryClassSectionInstructors.silent().take(-1).getItems()
        ]);
        this.logger.info(context, 'OneRosterSyncAction', filter, `Found ${results[0].length} instructor(s), ${results[1].length} student(s), ${results[2].length} class section(s) and ${results[3].length} class section instructor(s)`);

        const courseClassSections = results[2];
        const courseClassSectionInstructors = results[3];

        let instructorEnrollments = results[0].filter((x) => {
            if (x.indivisible) {
                return true;
            }
            return courseClassSections.filter((y) => y.classCode === x.classCode).length === 0;
        }).map((instructor) => {
            return {
                user: instructor.user,
                class: instructor.class,
                school: instructor.school,
                type: 'teacher',
                role: 'teacher',
                status: 'active',
                primary: true,
                beginDate: null,
                endDate: null
            }
        });

        const addInstructorEnrollments = [];
        for (const courseClassSection of courseClassSections) {
            const { classCode, section } = courseClassSection;
            courseClassSectionInstructors.filter((x) => {
                return x.classCode === classCode && x.section === section;
            }).forEach((item) => {
                addInstructorEnrollments.push({
                    user: item.user,
                    class: item.class,
                    school: item.school,
                    type: 'teacher',
                    role: 'teacher',
                    status: 'active',
                    primary: false,
                    beginDate: null,
                    endDate: null
                });
            });
        }

        if (filter.classSection) {
            instructorEnrollments = [];
        }
        instructorEnrollments.push(...addInstructorEnrollments);

        const studentEnrollments = results[1].map((student) => {
            const term = OneRosterAcademicSession.generateId(student.academicYear, student.academicPeriod);
            const originalClass = student.class;
            const item = {
                user: student.user,
                class: student.class,
                school: student.school,
                type: 'student',
                role: 'student',
                status: 'active',
                primary: false,
                beginDate: null,
                endDate: null,
                metadata: {
                    term,
                    originalClass
                }
            };
            return item;
        });

        const enrollments = [].concat(instructorEnrollments, studentEnrollments);
        this.logger.info(context, 'OneRosterEnrollmentProvider', filter, `Returning ${enrollments.length} enrollment(s)`);
        return [
            [
                'OneRosterEnrollment',
                enrollments
            ]
        ]
    }

    /**
     * @param {import('@themost/data').DataContext} context 
     * @param {{academicYear: number, academicPeriod: number, department: string | number, classCode?: string | number}} filter 
     * @returns 
     */
    async sync(context, filter) {
        // get classes
        const academicSession = OneRosterAcademicSession.generateId(filter.academicYear, filter.academicPeriod);
        const departmentIdentifier = `DEPT-${filter.department}`;
        const OneRosterClasses = context.model('OneRosterClass');
        // force model migration
        await context.model('OneRosterEnrollment').migrateAsync();
        let items = [];
        // emit event for filtering in-process classes
        const event = {
            target: this,
            entityType: 'OneRosterClass',
            items: [],
        }
        await this.filtering.emit(event);
        if (Array.isArray(event.items)) {
            items = event.items;
        } else {
            const queryOneRosterClasses = OneRosterClasses.select().where('terms/sourcedId').equal(academicSession).and('school/identifier').equal(departmentIdentifier);
            // if filter contains classCode
            if (filter.classCode) {
                // append class code filter
                queryOneRosterClasses.and('classCode').equal(filter.classCode);
            }
            // get any item
            items = await queryOneRosterClasses.take(-1).getItems();
        }
        const { sourceAdapter: source } = context.model('OneRosterEnrollment');
        // get course classes in order to validate if the current OneRoster class is a section
        const courseClasses = await context.model('CourseClass')
            .where('year').equal(filter.academicYear)
            .and('period').equal(filter.academicPeriod)
            .and('course/department').equal(filter.department)
            .select('id').getAllItems();
        const courseClassSections = await context.model('CourseClassSection')
            .where('courseClass/year').equal(filter.academicYear)
            .and('courseClass/period').equal(filter.academicPeriod)
            .and('courseClass/course/department').equal(filter.department)
            .select('courseClass', 'section').getAllItems();
        let index = 0;
        let total = items.length;
        const step = total > 1000 ? 100 : total > 100 ? 50 : 10;
        for (const item of items) {
            index++;
            if (index % step === 0) {
                // this.logger.info(context, 'OneRosterSyncAction', filter, `Synchronizing enrollments for class ${index} of ${total}`);
            }
            // get class title
            this.logger.info(context, 'OneRosterSyncAction', filter, `Synchronizing enrollments for (${item.classCode}) ${item.title}`);
            // clear class enrollments
            const [total] = await context.db.executeAsync(
                new QueryExpression().select(new QueryField().count('sourcedId').as('count')).from(source).where('class').equal(item.sourcedId)
            );
            this.logger.info(context, 'OneRosterSyncAction', filter, `Cleaning up ${total.count} enrollment(s) for (${item.classCode}) ${item.title} before continuing`);
            const deleteClassEnrollments = new QueryExpression().delete(source).where('class').equal(item.sourcedId);
            await context.db.executeAsync(deleteClassEnrollments);
            // is class or section?
            let { classCode } = item;
            let classSection;
            const findClass = courseClasses.find((x) => x.id === item.classCode);
            if (findClass == null) {
                // it's probably a section (remove section number at the end of classCode)
                const matches = /^(.*?)-(\d+)$/.exec(item.classCode);
                if (matches) {
                    const currentClassCode = matches[1];
                    const currentClassSection = parseInt(matches[2]);
                    const findSection = courseClassSections.find((x) => x.courseClass === currentClassCode && x.section === currentClassSection);
                    if (findSection) {
                        classCode = currentClassCode;
                        classSection = currentClassSection;
                    }
                }
            }
            // get class enrollments
            const enrollmentFilter = Object.assign({}, filter, {
                classCode: classCode,
                classSection: classSection,
                department: null
            })
            const results = await this.preSync(context, enrollmentFilter);
            const origin = context.getApplication().getConfiguration().getSourceAt('settings/universis/one-roster/origin') || '/ims/oneroster/v1p1/';
            const enrollments = results[0][1].map((item) => {
                // set base type and remove type
                item.baseType = 'enrollment';
                delete item.type;
                // set sourcedId and href
                item.sourcedId = item.sourcedId || uuidv4().toString();
                item.href = item.href || `${origin}enrollments/${item.sourcedId}`;
                // set date last modified if empty
                item.dateLastModified = item.dateLastModified || new Date();
                return item;
            });
            this.logger.info(context, 'OneRosterSyncAction', filter, `Preparing ${enrollments.length} enrollment(s) for (${item.classCode}) ${item.title}`);
            // save enrollments
            const OneRosterEnrollments = context.model('OneRosterEnrollment');
            for (const enrollment of enrollments) {
                // insert enrollment
                const insertEnrollment = OneRosterEnrollments.cast(enrollment, DataObjectState.Insert);
                await context.db.executeAsync(
                    new QueryExpression().insert(insertEnrollment).into(source)
                );
            }
            this.logger.info(context, 'OneRosterSyncAction', filter, `Finished synchronizing enrollments for (${item.classCode}) ${item.title}`);
        }
        if (total > 0) {
            this.logger.info(context, 'OneRosterSyncAction', filter, `Finished synchronizing enrollments for ${total} class(es)`);
        } else {
            this.logger.info(context, 'OneRosterSyncAction', filter, 'Finished synchronizing enrollments without getting any class');
        }
    }
}

class OneRosterNoEnrollmentProvider extends OneRosterProvider {
    constructor(app) {
        super(app);
    }
    // eslint-disable-next-line no-unused-vars
    async preSync(context, filter) {
        return [
            [
                'OneRosterEnrollment',
                []
            ]
        ];
    }
}

export {
    OneRosterEnrollment,
    OneRosterEnrollmentProvider,
    OneRosterEnrollmentWithSectionsProvider,
    OneRosterNoEnrollmentProvider
};
