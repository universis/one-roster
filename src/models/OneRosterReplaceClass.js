import { DataObject, EdmMapping } from '@themost/data';
import { OneRosterProvider } from './OneRosterProvider';
import { QueryEntity, QueryExpression, QueryField } from '@themost/query';
import { template } from 'lodash';

@EdmMapping.entityType('OneRosterReplaceClass')
class OneRosterReplaceClass extends DataObject {
    
}

class OneRosterReplaceClassProvider extends OneRosterProvider {
    constructor(context) {
        super(context);
    }

    /**
     * @param {import('@themost/data').DataContext} context 
     * @param {{academicYear: number, academicPeriod: number, department: string | number, classCode?: string | number }} filter 
     * @returns 
     */
    // eslint-disable-next-line no-unused-vars
    async preSync(context, filter) {
        return [
            [
                'OneRosterEnrollment',
                []
            ]
        ]
    }

    /**
     * @param {import('@themost/data').DataContext} context 
     * @param {{academicYear: number, academicPeriod: number, department: string | number, classCode?: string | number }} filter 
     * @returns 
     */
    // eslint-disable-next-line no-unused-vars
    async sync(context, filter) {

        const { viewAdapter: OneRosterReplaceClasses } = context.model('OneRosterReplaceClass');
        const { sourceAdapter: OneRosterClasses } = context.model('OneRosterClass');
        const { sourceAdapter: OneRosterEnrollments } = context.model('OneRosterEnrollment');
        const oneRosterReplacedBy = new QueryEntity(OneRosterClasses).as('oneRosterReplacedBy');
        const oneRosterClass = new QueryEntity(OneRosterClasses).as('oneRosterClass');

        // get replaced classes
        // pseudo SQL: SELECT * FROM OneRosterReplaceClass WHERE 
        // academicYear = ? AND academicPeriod = ? AND department = ? AND classCode = ?
        const queryClasses = context.model('OneRosterReplaceClass')
            .where('courseClass/year').equal(filter.academicYear)
            //.and('courseClass/period').equal(filter.academicPeriod) // remove period filter
        if (Object.prototype.hasOwnProperty.call(filter, 'classCode')) {
            queryClasses.prepare().where('courseClass').equal(filter.classCode).or('replacedBy').equal(filter.classCode);
        }
        queryClasses.select(
            'courseClass',
            'replacedBy',
            'courseClass/title as title',
            'courseClass/year/id as academicYear',
            'courseClass/year/name as academicYearName',
            'courseClass/period/id as academicPeriod',
            'courseClass/period/name as academicPeriodName',
            'courseClass/course/displayCode as displayCode',
            'courseClass/status/alternateName as status'
        );
        // pseudo SQL: JOIN OneRosterClass AS replacedBy ON replacedBy.classCode = OneRosterReplaceClass.replacedBy
        queryClasses.query.join(oneRosterReplacedBy).with(
            new QueryExpression().where(
                new QueryField('replacedBy').from(OneRosterReplaceClasses)
            ).equal(
                new QueryField('classCode').from(oneRosterReplacedBy)
            )
        ).join(oneRosterClass).with( 
            // pseudo SQL: JOIN OneRosterClass AS courseClass ON courseClass.classCode = OneRosterReplaceClass.courseClass
            new QueryExpression().where(
                new QueryField('courseClass').from(OneRosterReplaceClasses)
            ).equal(
                new QueryField('classCode').from(oneRosterClass)
            )
        );
        
        /**
         * @type {Array<QueryField>}
         */
        const selectClasses = queryClasses.query.$select[OneRosterReplaceClasses];
        // pseudo SQL: SELECT OneRosterReplaceClass.*, courseClass.sourcedId AS oneRosterClass,
        // replacedBy.sourcedId AS replacedByClass
        selectClasses.push(new QueryField('sourcedId').from(oneRosterClass).as('oneRosterClass'));
        selectClasses.push(new QueryField('title').from(oneRosterClass).as('oneRosterClassTitle'));
        selectClasses.push(new QueryField('sourcedId').from(oneRosterReplacedBy).as('replacedByClass'));
        selectClasses.push(new QueryField('title').from(oneRosterReplacedBy).as('replacedByClassTitle'));

        const replaceClasses = await queryClasses.orderBy(
            'courseClass/year/id',
            'courseClass/period/id'
        ).getAllItems();
        if (replaceClasses.length === 0) {
            return;
        }
        // get in-process classes
        const event = {
            target: this,
            entityType: 'OneRosterClass',
            items: [],
        }
        await this.filtering.emit(event);
        if (event.items.length === 0) {
            return;
        }
        this.logger.info(context, 'OneRosterSyncAction', filter, `Replacing classes`);

        const translateService = context.application.getStrategy(function TranslateService() { });
        const titleTemplate = template(
            context.getConfiguration().getSourceAt('settings/universis/one-roster/classTitleFormat') ||
            '${title}')

        for (const item of event.items) {
            const replaceClass = await replaceClasses.find((x) => x.courseClass === item.classCode);
            if (replaceClass == null) {
                continue;
            }
            const { oneRosterClass, oneRosterClassTitle, replacedByClass, replacedByClassTitle, courseClass, replacedBy } = replaceClass;
            this.logger.info(context, 'OneRosterSyncAction', filter, `Replacing class ${item.classCode} ${item.title}`);
            // deactivate class
            replaceClass.status = 'inactive';
            replaceClass.statusDesc = translateService.translate('inactive');
            // pseudo SQL: UPDATE OneRosterClass SET status = 'inactive' WHERE sourcedId = ?
            await context.db.executeAsync(new QueryExpression().update(OneRosterClasses).set({
                status: 'inactive',
                title: titleTemplate(replaceClass).trim()
            }).where('sourcedId').equal(oneRosterClass));
            const count = await context.model('OneRosterEnrollment')
                .where('class').equal(oneRosterClass)
                .and('role').equal('student')
                .silent().count();
            if (count > 0) {
                this.logger.info(context, 'OneRosterSyncAction', filter, `Moving ${count} student(s) from (${courseClass}) ${oneRosterClassTitle} to (${replacedBy}) ${replacedByClassTitle}`);
                // delete existing enrollments (already moved by previous sync)
                // pseudo SQL: SELECT sourcedId FROM OneRosterEnrollment WHERE class = ?
                // and JSON_VALUE(metadata, '$.originalClass') = ? and role = 'student'
                const { query: selectEnrollments }  = context.model('OneRosterEnrollment')
                    .select('sourcedId')
                    .where('class').equal(replacedByClass)
                    .and('metadata/originalClass').equal(oneRosterClass)
                    .and('role').equal('student');
                const enrollments = await context.db.executeAsync(selectEnrollments);
                if (enrollments.count === 0) {
                    this.logger.info(context, 'OneRosterSyncAction', filter, `Removing ${enrollments.count} duplicate enrolment(s) from (${replacedBy}) ${replacedByClassTitle} before moving enrolments from (${courseClass}) ${oneRosterClassTitle}`);
                }
                // execute delete duplicates
                await context.db.executeAsync(new QueryExpression().delete(OneRosterEnrollments).where('sourcedId').in(selectEnrollments));
                // move enrollments
                // pseudo SQL: UPDATE OneRosterEnrollment SET class = ? WHERE class = ?
                await context.db.executeAsync(new QueryExpression().update(OneRosterEnrollments).set({
                    'class': replacedByClass
                }).where('class').equal(oneRosterClass).and('role').equal('student'));
            }
        }

    }

}

export {
    OneRosterReplaceClass,
    OneRosterReplaceClassProvider
}