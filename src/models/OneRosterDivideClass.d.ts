import { ApplicationBase } from '@themost/common';
import { DataObject } from '@themost/data';
import { OneRosterProviderFilter } from './OneRosterBaseType';
import { OneRosterProvider } from './OneRosterProvider';

export interface OneRosterDivideClassFilter extends OneRosterProviderFilter {
    classCode?: string;
}

export declare class OneRosterDivideClass extends DataObject {
    //
}

export declare class OneRosterDivideClassProvider extends OneRosterProvider {
    constructor(app: ApplicationBase);
    preSync(context: DataContext, filter: OneRosterDivideClassFilter): Promise<any[][]>;
    sync(context: DataContext, filter: OneRosterDivideClassFilter): Promise<void>;
}
