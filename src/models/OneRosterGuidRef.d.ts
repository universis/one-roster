import { ApplicationService, ApplicationBase } from '@themost/common';
import { DataObject } from '@themost/data';

export declare class OneRosterGuidRef extends DataObject {
    sourcedId: string;
    type: string;
    href: string;
    static selectFrom(attribute: string): string
}
