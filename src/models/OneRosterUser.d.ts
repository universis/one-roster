import { ApplicationBase } from '@themost/common';
import { DataObject } from '@themost/data';
import { OneRosterProviderFilter } from './OneRosterBaseType';
import { OneRosterProvider } from './OneRosterProvider';

export declare class OneRosterUser extends DataObject {
    //
}


export interface OneRosterUserFilter extends OneRosterProviderFilter {
    classCode?: string;
}

export declare class OneRosterUserProvider extends OneRosterProvider {
    constructor(app: ApplicationBase)
    preSync(context: DataContext, filter: OneRosterUserFilter): Promise<any[][]>;
}

export declare class OneRosterNoUserProvider extends OneRosterProvider {
    constructor(app: ApplicationBase)
    preSync(context: DataContext, filter: OneRosterUserFilter): Promise<any[][]>;
}