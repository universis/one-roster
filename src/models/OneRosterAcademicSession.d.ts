import { ApplicationService, ApplicationBase } from '@themost/common';
import { DataObject } from '@themost/data';
import { OneRosterProviderFilter } from './OneRosterBaseType';
import { OneRosterProvider } from './OneRosterProvider';

export declare class OneRosterAcademicSession extends DataObject {
    //
}

export declare class OneRosterAcademicSessionProvider extends OneRosterProvider {
    constructor(app: ApplicationBase)
    preSync(context: DataContext, filter: OneRosterProviderFilter): Promise<any[][]>;
}