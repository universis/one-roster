import { ApplicationService, ApplicationBase } from '@themost/common';
import { DataObject } from '@themost/data';
import { OneRosterProviderFilter } from './OneRosterBaseType';
import { OneRosterProvider } from './OneRosterProvider';

export declare class OneRosterCourse extends DataObject {
    //
}

export declare interface OneRosterCourseProviderFilter {
    academicYear: any;
    academicPeriod: any;
    department?: any;
    courseCode?: string;
}

export declare class OneRosterCourseProvider extends OneRosterProvider {
    constructor(app: ApplicationBase)
    preSync(context: DataContext, filter: OneRosterCourseProviderFilter, options?: { force?:boolean, description?: string }): Promise<any[][]>;
}