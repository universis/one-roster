import { ApplicationService, ApplicationBase } from '@themost/common';
import { DataObject } from '@themost/data';
import { OneRosterAcademicSession } from './OneRosterAcademicSession';
import { OneRosterBaseType } from './OneRosterBaseType';
import { OneRosterProviderFilter } from './OneRosterBaseType';
import { OneRosterClass } from './OneRosterClass';
import { OneRosterLineItemCategory } from './OneRosterLineItemCategory';
import { OneRosterProvider } from './OneRosterProvider';

export declare class OneRosterLineItem extends OneRosterBaseType {
    title?: string;
    description?: string;
    assignDate?: Date;
    dueDate?: Date;
    class?: OneRosterClass | string;
    category?: OneRosterLineItemCategory | string;
    gradingPeriod?: OneRosterAcademicSession | string;
    resultValueMin?: number;
    resultValueMax?: number;
}

export declare class OneRosterLineItemProvider extends OneRosterProvider {
    constructor(app: ApplicationBase)
    preSync(context: DataContext, filter: OneRosterProviderFilter): Promise<any[][]>;
}

export declare class OneRosterNoLineItemProvider extends OneRosterProvider {
    constructor(app: ApplicationBase)
    preSync(context: DataContext, filter: OneRosterProviderFilter): Promise<any[][]>;
}