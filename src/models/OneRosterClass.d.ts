import { ApplicationService, ApplicationBase } from '@themost/common';
import { DataContext, DataObject } from '@themost/data';
import { OneRosterProviderFilter } from './OneRosterBaseType';
import { OneRosterProvider } from './OneRosterProvider';

export declare class OneRosterClass extends DataObject {
    //
}

export declare class OneRosterClassProvider extends OneRosterProvider {
    constructor(app: ApplicationBase)
    preSync(context: DataContext, filter: OneRosterProviderFilter, options?: { force?:boolean, description?: string, initiator: any }): Promise<any[][]>;
}