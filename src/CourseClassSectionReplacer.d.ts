import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';

export declare class CourseClassSectionReplacer extends ApplicationService {
    constructor(app: ExpressDataApplication);
}